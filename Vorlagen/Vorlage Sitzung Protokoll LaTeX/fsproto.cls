%% start of file `fsproto.cls'.
%% Copyright 2012 Ferdinand Schwenk (ferdisdot@gmail.com).
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License version 1.3c,
% available at http://www.latex-project.org/lppl/.


%-------------------------------------------------------------------------------
% identification
%-------------------------------------------------------------------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fsproto}[2013/08/01 v0.1 Class for 'fachschafts'-protocol]


%-------------------------------------------------------------------------------
% debugging
%-------------------------------------------------------------------------------
\newif\if@DEBUG\@DEBUGfalse


%-------------------------------------------------------------------------------
% option processing
%-------------------------------------------------------------------------------
% pass unknown options to scrartcl
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}

% execute default options
\ExecuteOptions{12pt,a4paper,oneside,pagesize,final}

% process given options
\ProcessOptions\relax


%-------------------------------------------------------------------------------
% load base-class
%-------------------------------------------------------------------------------
\LoadClass{scrartcl}


%-------------------------------------------------------------------------------
% required packages
%-------------------------------------------------------------------------------
% geometry package
%\RequirePackage{geometry}
%\geometry{hmargin=13mm}
%\geometry{vmargin=25mm}
%\geometry{top=21mm}
%\geometry{bottom=25mm}

% Sprachverwaltung
\RequirePackage{babel}

% vector-font package
\RequirePackage{lmodern}

% ifthen package
\RequirePackage{ifthen}

% ifpdf package
\RequirePackage{ifpdf}

% fontenc package
\RequirePackage[T1]{fontenc}

% paralist package
\RequirePackage{paralist}

% color
%\RequirePackage{color}

% hyperrefs
\RequirePackage{url}
\ifpdf
  \RequirePackage[pdftex,]{hyperref}
\else
  \RequirePackage[dvips]{hyperref}
\fi
\hypersetup{%
  breaklinks,%
  baseurl       = http://,%
  pdfborder     = 0 0 0,%
  pdfpagemode   = UseNone,%
  pdfcreator    = \LaTeX{} with `fsproto' class,%
  pdfproducer   = \LaTeX{}
}
\AtEndOfClass{%
  \AtBeginDocument{%
    \hypersetup{%
      pdfauthor     = \@author,%
      pdftitle      = \@title,%
      pdfsubject    = sdaps report \@title,%
      pdfkeywords   = sdaps report \@title%
    }%
  }%
}
\usepackage{scrhack}

%% graphics
%\ifpdf
%  \RequirePackage[pdftex]{graphicx}
%\else
%  \RequirePackage[dvips]{graphicx}
%\fi

% headers and footers
\usepackage{scrlayer-scrpage}
\clearscrheadings
  \ofoot{\@date\ -\ \jobname}
  \cfoot{\thepage/\pageref{LastPage}}
\renewcommand{\headfont}{\normalfont\sffamily}

\pagestyle{scrheadings}

%% table of fixed width
%\RequirePackage{tabularx}
%% display content vertical centered
%\renewcommand\tabularxcolumn[1]{m{#1}}
%\newcolumntype{Y}{>{\raggedleft}X}

% Einheiten
%\RequirePackage[dp=1]{siunitx}

%% Listen
%\RequirePackage{mdwlist}

% Stringfunktionen
\RequirePackage{xstring}

% Euro-Fix; we need to load UTF-8 first
\usepackage[utf8]{inputenc}
\usepackage{eurosym}
\DeclareUnicodeCharacter{20AC}{\euro}

% Checkbox symbol fpr TODOs
\RequirePackage{amssymb}


%-------------------------------------------------------------------------------
%                class definition
%-------------------------------------------------------------------------------
% minimal base settings
\setlength\lineskip{1\p@}
\setlength\normallineskip{1\p@}
\renewcommand\baselinestretch{}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt}
\setlength\columnsep{10\p@}
\setlength\columnseprule{0\p@}
\pagestyle{scrheadings}
\pagenumbering{arabic}
\raggedbottom
\onecolumn


%-------------------------------------------------------------------------------
%                style commands definitions
%-------------------------------------------------------------------------------
\def\smallskip{\vspace\smallskipamount}
\def\medskip{\vspace\medskipamount}
\def\bigskip{\vspace\bigskipamount}
\newskip\smallskipamount \smallskipamount=3pt  plus 1pt minus 1pt
\newskip\medskipamount   \medskipamount  =6pt  plus 2pt minus 2pt
\newskip\bigskipamount   \bigskipamount  =12pt plus 4pt minus 4pt
\parskip 0.5em

%%% From File: scrkvars.dtx
\newcommand*{\newkomavar}{%
  \@ifstar {\@tempswatrue\@newkomavar}{\@tempswafalse\@newkomavar}}
\newcommand*{\@newkomavar}[2][\relax]{%
  \@ifundefined{scr@#2@name}{%
    \@ifundefined{scr@#2@var}{%
      \begingroup
        \def\@tempa{#1}\def\@tempb{\relax}%
        \ifx\@tempa\@tempb\endgroup\else
        \endgroup\@namedef{scr@#2@name}{#1}%
      \fi%
      \expandafter\let\csname scr@#2@var\endcsname=\@empty
      \if@tempswa\addtoreffields{#2}\fi
    }{%
      \ClassError{%
        scrlttr2%
      }{%
        This should never happen%
      }{%
        The contents of the KOMA-Script variable `#2'\MessageBreak
        is undefined, but the name of the variable is
        defined.\MessageBreak
        This should never happen. So someone crashs me!%
      }%
    }%
  }{%
    \ClassError{%
      scrlttr2%
    }{%
      Variable `#2' already defined%
    }{%
      I'll ignore this command, if you'll continue.%
    }%
  }%
}
\newcommand*{\setkomavar}{%
  \@ifstar {\@setkomaname}{\@setkomavar}}
\newcommand*{\@setkomavar}[1]{%
  \@ifnextchar [%]
  {\@@setkomavar{#1}}{\@@setkomavar{#1}[\relax]}}
\newcommand*{\@setkomaname}[2]{%
  \@ifundefined{scr@#1@var}{%
    \@komavar@err{set}{#1}%
  }{%
    \@namedef{scr@#1@name}{#2}%
    \csname scr@#1@postsetname\endcsname
  }%
}
\newcommand*{\@@setkomavar}{}
\long\def\@@setkomavar#1[#2]#3{%
  \@ifundefined{scr@#1@var}{%
    \@komavar@err{set}{#1}%
  }{%
    \@namedef{scr@#1@var}{#3}%
    \csname scr@#1@postsetvar\endcsname
    \begingroup
      \def\@tempa{#2}\def\@tempb{\relax}%
      \ifx\@tempa\@tempb\endgroup\else
      \endgroup\@namedef{scr@#1@name}{#2}%
      \csname scr@#1@postsetname\endcsname
    \fi
  }%
}
\newcommand*{\@komavar@err}[2]{%
  \ClassError{%
    scrlttr2%
  }{%
    KOMA-Script variable not defined%
  }{%
    You've tried to #1 the not defined KOMA-Script variable
    `#2'.\MessageBreak
    You have to define the variable using \string\newkomavar\space
    before\MessageBreak
    you do this.%
  }%
}
\DeclareRobustCommand*{\usekomavar}{%
  \@ifstar {\@usekomaname}{\@usekomavar}}
\newcommand*{\@usekomavar}[2][\@firstofone]{%
  \@ifundefined{scr@#2@var}{%
    \@komavar@err{use}{#2}%
  }{%
    #1{\@nameuse{scr@#2@var}}%
  }%
}
\newcommand*{\@usekomaname}[2][\@firstofone]{%
  \@ifundefined{scr@#2@var}{%
    \@komavar@err{use}{#2}%
  }{%
    #1{\@nameuse{scr@#2@name}}%
  }%
}
\DeclareRobustCommand*{\ifkomavar}[1]{%
  \scr@ifundefinedorrelax{scr@#1@var}{%
    \expandafter\@secondoftwo
  }{%
    \expandafter\@firstoftwo
  }%
}
\DeclareRobustCommand*{\ifkomavarempty}{%
  \begingroup
  \@ifstar {\@tempswatrue\@ifkomavarempty}%
           {\@tempswafalse\@ifkomavarempty}%
  }
\newcommand{\@ifkomavarempty}[1]{%
    \ifkomavar{#1}{%
      \if@tempswa
        \@ifundefined{scr@#1@name}{%
          \aftergroup\@firstoftwo
        }{%
          \expandafter\ifx\csname scr@#1@name\endcsname\@empty
            \aftergroup\@firstoftwo
          \else
            \aftergroup\@secondoftwo
          \fi
        }%
      \else
        \expandafter\ifx\csname scr@#1@var\endcsname\@empty
          \aftergroup\@firstoftwo
        \else
          \aftergroup\@secondoftwo
        \fi
      \fi
    }{%
      \@komavar@err{use}{#1}%
      \aftergroup\@gobbletwo
    }%
  \endgroup
}

%-------------------------------------------------------------------------------
%                structure commands definitions
%-------------------------------------------------------------------------------
\renewcommand{\author}[1]{\def\@author{#1}}
\renewcommand{\title}[1]{\def\@title{#1}}
\def\@title{\usekomavar{sitzungsname} vom \@date}

\newcommand{\@fsproto@format@list}[2][ und]{%
  \IfSubStr{#2}{,}{%
    \StrCount{#2}{,}[\@fsproto@list@pos]%
    \StrBefore[\@fsproto@list@pos]{#2}{,}[\@fsprot@list@anf]%
    \StrBehind[\@fsproto@list@pos]{#2}{,}[\@fsprot@list@end]%
    \@fsprot@list@anf{}#1\@fsprot@list@end%
  }{%
    #2%
  }%
}
\newcommand{\anwesend}[1]{\setkomavar{anwesend}{\@fsproto@format@list{#1}}}
\newcommand{\anwesendgaeste}[1]{%
  \IfSubStr{#1}{,}{%
    \setkomavar*{gaeste}{G\"aste:}%
  }{%
    \setkomavar*{gaeste}{Gast:}%
  }%
  \setkomavar{gaeste}{\@fsproto@format@list{#1}}%
}
\newcommand*{\kommt}{\@ifstar{\@kommtpl}{\@kommt}}
\newcommand{\@kommt}[2]{\begin{description}\item[#1~Uhr] #2 kommt.\end{description}}
\newcommand{\@kommtpl}[2]{\begin{description}\item[#1~Uhr] \@fsproto@format@list{#2} kommen.\end{description}}
\newcommand*{\kommtgast}{\@ifstar{\@kommtgaeste}{\@kommtgast}}
\newcommand{\@kommtgast}[2]{\begin{description}\item[#1~Uhr] #2 (Gast) kommt.\end{description}}
\newcommand{\@kommtgaeste}[2]{\begin{description}\item[#1~Uhr] \@fsproto@format@list{#2} (G\"aste) kommen.\end{description}}
\newcommand*{\geht}{\@ifstar{\@gehtpl}{\@geht}}
\newcommand{\@geht}[2]{\begin{description}\item[#1~Uhr] #2 geht.\end{description}}
\newcommand{\@gehtpl}[2]{\begin{description}\item[#1~Uhr] \@fsproto@format@list{#2} gehen.\end{description}}
\newcommand*{\gehtgast}{\@ifstar{\@gehtgaeste}{\@gehtgast}}
\newcommand{\@gehtgast}[2]{\begin{description}\item[#1~Uhr] #2 (Gast) geht.\end{description}}
\newcommand{\@gehtgaeste}[2]{\begin{description}\item[#1~Uhr] \@fsproto@format@list{#2} (G\"aste) gehen.\end{description}}
\newcommand*{\protokollant}{\@ifstar{\@protokollantin}{\@protokollant}}
\newcommand{\@protokollant}[1]{\setkomavar{protokollant}[Protokollant:]{#1}}
\newcommand{\@protokollantin}[1]{\setkomavar{protokollant}[Protokollantin:]{#1}}
\newcommand{\sitzungsleitung}[1]{\setkomavar{sitzungsleitung}{#1}}
\newcommand{\sitzungsname}[1]{\setkomavar{sitzungsname}{#1}}
\newcommand{\fachschaft}{\usekomavar{fachschaft}}
\newcommand{\sumpf}{\usekomavar{sumpf}}
\newcommand{\TOP}[2]{\setcounter{section}{#1}\addsec{TOP #1: #2}\label{top:#1}}
\newcommand{\AntragTop}[3]{#1 beantragt den \textbf{TOP #2: #3}.}
\newcommand{\@fsproto@antwort}[2]{%
  \global\edef\@fsproto@abstimmung@antw{\@fsproto@abstimmung@antw \@fsproto@abstimmung@sep #1}%
  \global\edef\@fsproto@abstimmung@anz{\@fsproto@abstimmung@anz \@fsproto@abstimmung@sep #2}%
  \global\def\@fsproto@abstimmung@sep{/}%
}
\newenvironment{Abstimmung}[1]{%
  \par\noindent%
  \textbf{Abstimmung: \textit{#1}}%
  \global\def\@fsproto@abstimmung@sep{}%
  \global\def\@fsproto@abstimmung@antw{}%
  \global\def\@fsproto@abstimmung@anz{}%
  \global\let\antwort\@fsproto@antwort%
}{%
  \vspace*{-2\parskip}%
  \begin{flushright}%
    \textbf{\@fsproto@abstimmung@antw:} \@fsproto@abstimmung@anz%
  \end{flushright}%
  \global\let\antwort\@undefined%
}
\newcommand{\abstimmung}[4]{\begin{Abstimmung}{#1}\antwort{Ja}{#2}\antwort{Nein}{#3}\antwort{Enth.}{#4}\end{Abstimmung}}
\newcommand{\pause}[2]{\begin{description}\item[#1~Uhr] Die Sitzung wird unterbrochen.\item[#2~Uhr] Die Sitzung wird fortgesetzt.\end{description}}

\let\@tasks\@undefined
%\newcommand{\@task@protokoll}[3]{%
%  \begin{description}%
%    \item[#2]%
%    \ifthenelse{\equal{#1}{}}{\leavevmode}{ -- Erledigen bis #1}%
%    \par%
%    #3%
%  \end{description}%
%}
\newcommand{\@task@protokoll}[3]{%
  \begin{description}%
    \item[Aufgabe:] #2%
    \ifthenelse{\equal{#1}{}}{}{ -- Erledigen bis #1}%
    \par%
    #3%
  \end{description}%
}
\newcommand{\@task@tasklist}[3]{%
    \item \textbf{#2}%
    \ifthenelse{\equal{#1}{}}{}{ -- Erledigen bis #1}%
    \par%
    #3%
}
\newcommand{\task}[3][]{%
  \@task@protokoll{#1}{#2}{#3}%
  \ifx\@tasks\@undefined%
    \global\def\@tasks{}%
  \else%
  \fi%
  \g@addto@macro\@tasks{\@task@tasklist{#1}{#2}{#3}}%
}

\newkomavar{anwesend}
\setkomavar*{anwesend}{Anwesend:}
\setkomavar{anwesend}{}
\newkomavar{gaeste}
\setkomavar*{gaeste}{G\"aste:}
\setkomavar{gaeste}{}
\newkomavar{protokollant}
\setkomavar*{protokollant}{Protokollant:}
\setkomavar{protokollant}{}
\newkomavar{sitzungsleitung}
\setkomavar*{sitzungsleitung}{Sitzungsleitung:}
\setkomavar{sitzungsleitung}{}

\newkomavar{sitzungsname}
\setkomavar{sitzungsname}{Fachschaftssitzung}

\newkomavar{fachschaft}
\setkomavar{fachschaft}{Fachschaft Elektro- und Informationstechnik des Karlsruher Instituts für Technologie}

\newkomavar{sumpf}
\setkomavar{sumpf}{Fachschaftsvorraum}

\renewcommand*\maketitle[2][1]{
  \setcounter{page}{#1}%
  {\centering\huge\usekomafont{title}\@title\\}
%  \begin{titlepage}%
%    \null\vfill
%    \begin{center}%
%      \ifx\@subject\@empty \else
%        {\Large \@subject \par}%
%        \vskip 3em
%      \fi
%      {\titlefont\huge \@title\par}%
%      \vskip 3em
%      {\Large \lineskip 0.75em
%      \begin{tabular}[t]{c}%
%        \@author
%      \end{tabular}\par}%
%      \vskip 1.5em
%      {\Large \@date \par}%
%    \end{center}
%    \vskip \z@ \@plus3fill
%    \begin{tabular}[t]{rl}%
%      \@extrainfos
%    \end{tabular}%
%  \end{titlepage}%
  \begin{description}
    \item[\usekomavar*{anwesend}] \usekomavar{anwesend}
    \ifkomavarempty{gaeste}
    {}
    {
      \item[\usekomavar*{gaeste}] \usekomavar{gaeste}
    }
    \item[Begin:] #2
  \end{description}
  \global\let\maketitle\relax
  \global\let\@author\@empty
%  \global\let\@date\@empty
%  \global\let\@title\@empty
  \global\let\@subject\@empty
  \global\let\@extrainfos\@empty
  \global\let\author\relax
  \global\let\title\relax
  \global\let\subject\relax
  \global\let\extrainfos\relax
  \global\let\date\relax
  \global\let\and\relax
}

\newcommand{\makeclosing}[1]{
  \begin{description}
    \item[Ende:] #1
  \end{description}
  \vspace*{3em}
  \begin{tabbing}
    \hspace*{0.5\linewidth}\=\kill
    \ifkomavarempty{sitzungsleitung}
    {
      \usekomavar*{protokollant} \usekomavar{protokollant}
    }{
      \usekomavar*{protokollant} \usekomavar{protokollant} \> \usekomavar*{sitzungsleitung} \usekomavar{sitzungsleitung}
    }
  \end{tabbing}

  \label{LastPage}

  \ifx\@tasks\@undefined
  \else
    \clearpage{}
    \cfoot{}
    \addsec{Liste der Aufgaben:}
    \begin{itemize}[$\square$]
      \@tasks
    \end{itemize}
  \fi
}

\endinput
