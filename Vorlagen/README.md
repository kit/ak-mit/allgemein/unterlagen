# Vorlagen

Dieser Ordner sammelt alle Vorlagen, die der AK-MIT so benötigt. 

## Protokollvorlagen

Die LaTeX Protokollvorlage wurde von der FS ETEC "inspiriert". Sie liegt sowohl als Ordner mit einzelnen Dateien vor, als auch als ZIP Datei für den bequemeren Import in Overleaf. 

