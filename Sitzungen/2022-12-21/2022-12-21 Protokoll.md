# AK-MIT Sitzungsprotokoll 21.12.2022

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Feline, Max

#### Anwesende

[comment]: # (AK-MITler)

Max, Thorben, Carl, Robin, Feline, Magdalena, Simon, Antonio



[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena

#### Sitzungsbeginn

13:02 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Die Annahme der Protokolle der letzten Sitzungen wurde vertagt.



## TOP 2: Berichte

#### FS-MACH/CIW

keine besonderen Ereignisse


#### FS-ETIT

keine besonderen Ereignisse

#### Weihnachtsfeier

Weihnachtsfeier hat gut funktioniert und kam gut an.

#### Prüfungsausschuss

Klärung über die Täuschungsversuche, warum kommen diese nicht in den PA?
bzw. wo landen diese Anträge?

Morgen Klärung in der PA-Sitzung.


#### StuKo

Maschienebau beachtet den MIT Studiengang bei der neuen SPO bisher nicht. Die StuKo denkt darüber nach, ggf. die MACH SPO abzulehnen und dadurch den Druck zu erhöhen. Die Institute müssten in diesem Fall nämlich die alten Vorlesungen parallel zu den neuen weiter anbieten.

Leider dadurch die Befürchtung, das die ECETS einfach nur runtergesetzt werden.
Vor dem 24h-Workshop mit den Studis im FakRat Mach besprechen und so schnell, wie möglich mit Prof. Gremier einen Termin ausmachen.

Bei der mündliche Prüfung darf die schriftliche Prüfung mit beachtet werden. Bei einem Täuschungsversuch, in der zweiten schriftlichen Prüfung, ist das Recht auf eine Nachprüfung verwirkt.
Bezüglich der Online-Prüfung wurde die Änderung abgelehnt.


## TOP 3: LAZ

(Lern- und Arbeitszentrum)

Es kam nochmal eine E-Mail. Zuständige sollen sich überlegen, wie man es gestalten soll.
Eine Einrichtung für einen Mailverteiler dafür und Berichte auf den Fachschaftssitzungen.


## TOP 4: AK-MIT Webseite

Neuer Beauftragter für die Webseite: Antonio.


## TOP 5: Jahrgangstreffen MSuP

Es kam eine Mail von einem der Veranstalter von MSUP.
Der Verteiler, der weitergeleitet wird soll der Verteiler von allen Mechtronikstudierenden sein. 

Robin macht die Orga für die Feier am 3.2.2022 nach dem Wettbewerb zu organisieren.


## TOP 6: Survival ETIT

Es kam eine Mail mit den Folien für die Veranstaltung. Folien bitte einmal anschauen und antworten.


## TOP 7: Sonstiges

- Mail bzgl. Bachelorarbeitverlängerung wird beantwortet, Leitfaden weiterleiten; Carl beantwortet die E-Mail

- Besprechen vom Zusatzbereich in den Wahlbereich verschieben, dies soll morgen besprochen werden

**Nächste Sitzung am Mittwoch, 11.01.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:45 Uhr
