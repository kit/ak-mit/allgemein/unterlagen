# AK-MIT Sitzungsprotokoll 11.01.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Thorben

#### Anwesende

[comment]: # (AK-MITler)

Magdalena, Robin, Thorben, Leo, Antonio

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena 

#### Sitzungsbeginn

13:07 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Thorben begrüßt alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Protokollannahme wurde vertagt.



## TOP 2: Berichte

#### FS-MACH/CIW

Diese Woche Winterhütte, einer der Workshops ist über die SPO Mach. 
Abstimmung über die Online-Prüfung wurde vertagt, aber Meinungsbild war für Ablehnung. Satzungsänderung zur mündlichen Nachprüfung wurde angenommen.


#### FS-ETIT



Konklusion:
Die Abstimmungen wurden jeweils unterschiedlich abgestimmt bzgl. der online-Prüfungen und der Täuschungsversuchen, mal schauen was dann für uns gilt.


## TOP 3: AK LAZ

Ausflug zum Maker-Space am Campus Nord um sich das dort mal anzuschauen. Auf der Sitzung der FS ETIT wurde bereits berichtet. Bericht auf der Sitzung der FS MACH/CIW steht noch aus. 


## TOP 4: Wahlen/FüS

Unsere Sitzungsleitung spricht mit den Fachschaften für einen Termin. Damit wir z.B. Carl im nächsten Semester im BPA ersetzen und generell mal über die SPO reden können.


## TOP 5: Sonstiges

Nichts


**Nächste Sitzung am Mittwoch, 18.01.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:20 Uhr
