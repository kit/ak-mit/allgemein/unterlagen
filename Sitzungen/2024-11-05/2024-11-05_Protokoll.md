# AK-MIT Sitzungsprotokoll 05.11.2024

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Carla und Carl

#### Anwesende

[comment]: # (AK-MITler)

 Lorenz, Thorben, Fabian, Simon, Moritz, Matthias, Emilio, Max, Carla, Carl

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)

Thorsten, Marcel, Marco, Lukas, Jan, Vincent, Hannah

#### Protokollierende Person

Carl Bodmann
[comment]: # (Danke, dass du das Protokoll schreibst!)


#### Sitzungsbeginn

13:05
---

## TOP -1: Einführung AK MIT

## TOP 0: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

## TOP 1: O-Phase:
Kalender von FMC war übersichtlicher, Ort und co. stand klar mit dabei, das wurde bei ETIT vermisst.
MIT O-Phasen Kalender wurde nicht gefunden/genutzt. O-Phasen Kalender von FMC und ETIT wurden genutzt.

Infoveranstaltung MIT: sehr hilfreich, Folien gutes Nachschlagewerk.

In Keinpentour kam Feedback, dass MIT Kalender recht gut an kam

Informationsdilemma aus ETIT Sicht: 
-an MIT Erstis kam Mail recht spät raus, Infos wurden zu spät verschickt
-Material für MIT-Grillen wurde recht spät angefragt
-Mail: Auf Webseite standen recht lange Infos
-Ziel für nächstes Jahr: auf beiden FS Webseiten stehen MIT Infos

## TOP 2: Weihachtsfeier

-Gibt meistens Glühwein + Kekse

-Finanzierung + Ort in Klärung

-Orga: Simon, Fabian und Matthias


## TOP 3: Masterzugangskommission

Eine Runde mit HiWi, lief sehr gut

HiWi: Max + Thorben haben evtl. Interesse

HiWi macht bürokratische Arbeit, aber dafür gut bezahlt. Wird recht gut bezahlt

Interessenten: Max Berger, 

## TOP 4: Berichte
#### AK MIT
-alte Vertiefungsrichtung Medizintechnik macht Probleme: Fächer wurden zusammeengestrichen
-Prozess der Zeugniserstellung soll beschleunigt werden: kritisch vorallem, weil Zusatzelistungen wegfallen. Wollten an der Frist die Studi selber in der Hand hat schrauben. BEschleunigung der Zeugniserstellung + Reduzierung der Unterschriften sinnvoll. Geht jetzt Gremienwege
-In Kontakt mit Profs für neue Vertiefungsrichtungen im Master MIT ab SoSe 2025
-Nach der nächsten Stuko sollen Vertiefungsfächer + Inhalte informatiert werden dürfen


#### FS-MACH/CIW
-Winterfest am 06.12.
-Mitte Dezember Infoveranstaltung zur neuen MACH SPO
-Probleme mit TM1 Nachprüfungen: sehr kurzfristige Termine, keine Sprechstunden bei Prof, Mitarbeiter,... für inhaltliche Fragen


#### FS-ETIT
-Plant am 12.12. Winterfest auf dem Präsidiumsparkplatz
-am 19.11. und 26.11. soll es Infoveranstaltung zum neuen Master ETIT/MEDT geben
-auf letzter Stuko wurde ausführlich drüber dikutiert, dass durchschnittliche Studiendauer deutlich zu hoch ist. z.B. BA wirklich nur 3 Monate



## TOP 4:

## TOP 5: 

## TOP 6:

## TOP 7:









---

#### Sitzungsende




