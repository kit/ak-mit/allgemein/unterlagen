# AK-MIT Sitzungsprotokoll 23.11.2022

#### Sitzungsleitung

Max, Feline



#### Anwesende

[comment]: # (AK-MITler)
Max, Feline, Carl, Magdalena


[comment]: # (FS-MACH/CIW)



[comment]: # (FS-ETIT)



#### Protokollierende Person

Magdalena


#### Sitzungsbeginn

13:06

---

## TOP 0: TO-Annahme

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

Max nimmt noch Änderungen vor.


## TOP 2: Berichte

#### FS-MACH/CIW: 

nichts


#### FS-ETIT

Entscheidung für Weihnachtsfeier und für die Etit-Survivalveranstaltung. (Soll beides gemacht werden.)


## TOP 3: GitLab Verwaltung

* es gibt einen neuen Ordner Allgemeines

* Leitung teilt die AK-Mitler ein

* von jeder Fachschaft gibt es einen Admin, der die Fachschaftler einteilt

* wer auf was Zugriff hat, steht in den Folien

* PA mindestens mal die KIT-Emailadresse auf dem Emailverteiler

* pushen in der Gremien/Intern auf Gitlab soll möglich sein

## TOP 4: AK Moderne Klausuren

* siehe Folien

* Hintergrund: Klausuren sind nicht so gut, daraufhin Vorschläge für bessere Klausuren und auch Hiwi-Gelder dafür

* diese Vorsschläge für neue Klausuren werden hier vorgestellt (Vorschläge siehe Folien)

* Beispiele zu der Umsetzung in den Folien

* Berichte evtl. mal auf den Fachschftssitzung

* wird auf der Stuko MIT vorgestelllt

## TOP 5: Sonstiges

Nächste Sitzung am 30.11.2022


---

#### Sitzungsende

13:52 Uhr
