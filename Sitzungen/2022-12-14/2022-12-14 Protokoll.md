# AK-MIT Sitzungsprotokoll 14.12.2022

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max Zuidberg, Feline Pirchmoser

#### Anwesende

[comment]: # (AK-MITler)

Max, Feline, Robin, Magdalena, Carl

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Carl

#### Sitzungsbeginn

13:11 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Das überarbeitete Protokoll vom 16.11.2022, geschrieben von Thorben, wurde angenommen.<br>
Das überarbeitete Protokoll vom 07.12.2022, geschrieben von Magdalena, wurde angenommen. 

Die Annahme des Protokolls der Sitzung vom 23.11.2022, geschrieben von Magdalena, wurde wegen vertagt. Grund: fehlendes Review.<br>
Die Annahme des Protokolls der Sitzung vom 30.11.2022, geschrieben von Magdalena, wurde wegen vertagt. Grund: fehlendes Review.


## TOP 2: Berichte

#### FS-MACH/CIW

Heute (14.12) findet deren Weihnachtsfeier statt, alle Mechatroniker sind herzlich eingeladen.

#### FS-ETIT


## TOP 3: Sprechstunden

[comment]: # (Dieser TOP kommt nicht auf jeder Sitzung vor; bei Bedarf einfach löschen.)

[comment]: # (Nachfolgenden Satz anpassen falls nötig! Die FS wechselt i.d.R. jedes Semester.)

Die Sprechstunden finden jede Woche freitags um 12:30-13:30 Uhr in der FS ETIT statt. 

Besetzung der nächsten Sprechstunden:

- 16.12.2022: Carl
- 13.01.2022: Magdalena
- 20.01.2022: Magdalena
- 27.01.2022: Magdalena

Am 23.12.2022, 30.12.2022 und 06.12.2022 finden **keine** Sprechstunden statt. 

## TOP 4: PA: Zweitwiederholung

Diskussion, wie Zweitwiederholungsanträge in Zukunft effizienter gehandhabt werden sollen. Die Vorschläge sind vertraulich. Grundsätzlich befürwortet der AK-MIT entsprechende Vorschläge, wenn sie nicht zu Nachteilen für die Studierenden führt. 

## TOP 5: Weihnachtsfeier

Die Weihnachtsfeier findet im IBT 4. OG Raum 413.3 ab 17:30 Uhr statt. Es gibt Glühwein und Co. 
Glühwein wurde bereits besorgt, Punsch und Kekse sollen noch besorgt werden. Glühweinkocher werden über die FS MACH/CIW organisiert.
Robin ist ab 16 Uhr im Gebäude. Marco macht Waffelteig, Waffeleisen bringen Robin und evtl. Magdalena mit.
Es wird noch eine Info-Mail an die Institutsleitung geschickt.
Hörsaalwerbung in Höhere Mathematik vor der Weihnachtsfeier.

## TOP 6: Studierendennetzwerk 

Im LAZ soll Studierendennetzwerk aufgebaut werden. AK-MIT zieht das Ganze hoch. TOP wird vertagt.

## TOP 7: Sonstiges

Der Einladungslink der Ersti-WhatsApp-Gruppe wurde erneuert, wird nicht mehr veröffentlicht, hängt bei der Weihnachtsfeier nochmal aus. 

**Nächste Sitzung am Mittwoch, 21.12.2022 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:51 Uhr
