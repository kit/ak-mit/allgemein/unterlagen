# AK-MIT Sitzungsprotokoll 15.02.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max Zuidberg

#### Anwesende

##### AK-MITler

Max Zuidberg, Thorben, Robin,

##### FS-ETIT

Manuel

#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Thorben Bruns

#### Sitzungsbeginn

13:15 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg begrüßen alle Anwesenden.

Tagesordnung mit Änderungen per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Review und Annahme der Protokolle werden aufgrund technischer Probleme vertagt. 


## TOP 2: Berichte

#### FS-MACH/CIW

CIW Fakultät war nicht bewusst, dass MACH SPO-Anderungen macht

#### FS-ETIT

Fachschaftshütte steht, 12. bis 14. Mai 2023

## TOP 3: Master-Ophase

Master-O-Phase der ETITs (12. bis 16. April) in letzter vorlesungsfreier Woche. 
    
Geplant ist Zusammenschluss mit anderen Fachschaften.
    
Eigene Veranstaltung für Mechatronik ist wünschenswert.

Master-O-Phase MACH/CIW in erster Vorlesungswoche

Wir bleiben mit beiden Fachschaften im Austausch

## TOP 4: SPO MIT

### Treffen mit Prof. Böhlke und Prof. Fidlin

Treffen letzte Woche Mittwoch. Waren Änderungen gegenüber offen, wie wir das händeln. Wurde sehr pragmatisch gesehen.
    
Kapazität für eigene MIT Veranstaltung nicht da, sind felxibel für Anpassung ihrer Veranstaltungen für uns. 
    
Konkretes Beispiel für Teilung eines Fachs in einem Semester ist gewünscht.
    
Wir waren positiv überrascht.
    
Prof. Böhlke bevorzugt Gespräche in kleiner Runde. 

### Treffen mit Prof. Hillerbrand

vertagt

### Treffen der ETIT-Profs

noch keine Neuigkeiten. Wird nachgefragt

### Treffen mit Prof. Hohmann und Prof. Barth

vor eineinhalb Wochen spontanes Treffen mit beiden Profs. Treffen sehr positiv. Motivation bestand MIT Änderungen langfristig größer anzugehen. 
    
Überrascht von unserem Vorschlag. Würden den gerne noch weiter denken. Mehr Mechatronik spezifische Veranstaltungen sind gewüscht. 
    
Keine konrete Ergebnisse. Aber wir hatten ähnliche Ziele. Gespräch in größerer Runde gewünscht. 
Grundfrage war, wo wir und sie mit MIT hin möchten.
    
Konsens war, dass MIT nicht überall in Tiefe gehen muss (z.B. keine 21 ECTS TM nötig). 
Wir brauchen mehr Basisfächer für uns. Die Tiefe brauchen wir nicht
    
Zeitlich werden diese Änderungen nicht möglich sein. Unser vorgelegter Plan war für sie unter den Umständen hinnehmbar, aber mit weiteren Änderungen in den kommenden Jahren, um mehr eigene MIT Fächer zu starten
    
Vorschlag war, dass wir uns mit Prof. Sax treffen sollten, dies kam leider nicht zustande

### Allgemeine Neuigkeiten

Unsere SPO wird erst im Juli durch die SK POAZ gehen, da jetzt der Termin im März schon ausgebucht war. Dadurch erhalten wir mehr Zeit.

### Treffen mit Eisenmann bzgl. LAZ und MKL

MKL soll modularer werden. Idee war nicht Thema für Thema durchzugehen, sondern jedes Semester mehr in die Tiefe gehen. In jedem Semester würde jedes Thema behandelt werden, aber jedes Mal mit zusätzlichen Details oder aus einem anderen Blickwinkel. 

Keine eigene Veranstaltungen für CIW oder MIT. Stattdessen unterschiedliche Zusammensetzung aus modularen Themengebieten. Dadurch mehr Variation. 

Könnte zu Schwierigkeiten führen, organisatorisch für Studenten schwierig. Konzept steht und fällt mit der Organisation. 

Erstsemester Mechatronik Workshop im 1. Semester als eigene Veranstaltungen sieht man nicht. Im Rahmen des MKL Moduls kann man dafür ein oder zwei ECTS Vorsehen. Dadruch würden wir nicht die vollen 8 ECTS hören, sondern nur 6 oder 7. Der Rest ist im Workshop. 

Klausur ist dann auch modular gestaltet. 

Allgemeiner Einblick was aus Institutseite bei der neuen SPO MIT lief oder eben nicht. Parallel und ähnliche Vorlesungen finden sie doof. Enstehung von MIT kam und die Streitigkeiten wurden auch mal dargestellt. 


## TOP 5: AK-MIT Infrastuktur

Seit diesem Semester kriegen wir mehr Informationen von den Fachschaften an unseren info@ Verteiler. Bis jetzt standen dort einige mit Privatadressen drauf. Die FS ETIT hat daher aus Datenschutzgründen die Weitergabe von Informationen weitestgehend gestoppt. Inzwischen wurden alle Privatadressen entfernt und die FS ETIT leitet wieder alle internen Infos an uns weiter. <br>
Anmerkung: Die FS MACH/CIW hat von Anfang an lediglich die Sitzungseinladung an uns weitergeleitet und keinerlei FS-internen Informationen. 

Gespräch mit Dominik (FS ETIT Admin) bzgl. eines Umzugs der AK-MIT Mailverteiler. Bisher werden diese beim AStA betrieben, wobei sich die Kommunikation als eher schwierig und langwierig herausgestellt hat. Dominik sieht zwei Möglichkeiten: Umzug zur FS ETIT oder zum SCC.

SCC Vorteile

* Wir können uns selbst verwalten, sind nicht von Fachschaften abhängig.

SCC Nachteile

* Bei Problemen müssen wir uns an SCC Suport wenden. 
* Kopplung an u-Accounts statt Fachschaftsaccounts. 
* Umzug bedeutet auch Umstieg auf andere Software. 

FS ETIT Vorteile

* Kurzer Dienstweg, direkte Kommunikation. 
* Reibungsloser Umzug, unveränderter Weiterbetrieb möglich. 
* Ausgliederung auf einen eigenen Server denkbar; damit wären AK-MIT-spezifische Anpassungen möglich, unabhängig von der FS-Infrastruktur. 
* FS ETIT hat einen guten Draht zum SCC.

FS ETIT Nachteile

* Umzug erst möglich nachdem die FS ETIT ihren eigenen Website- und Serverumzug abgeschlossen hat. Dies dauert voraussichtlich noch min. ein Jahr. 
* Für den administrativen Zugang wäre zwingend ein FS ETIT Konto notwendig. 


## TOP 6: Sprechstunden

[comment]: # (Dieser TOP kommt nicht auf jeder Sitzung vor; bei Bedarf einfach löschen.)

[comment]: # (Nachfolgenden Satz anpassen falls nötig! Die FS wechselt i.d.R. jedes Semester.)

Sprechstunden in der vorlesungsfreien Zeit? In der vorlesungsfreien Zeit keine Sprechstunden des AK-MITs. Die Planung der Sprechstunden im Sommersemester wird auf den ersten Sitzungen des Semester durchgeführt. 

Viele Mechatroniker kommen Freitags zu unseren Zeiten in die FS MACH/CIW. Vorschlag: Auf Faschaftstüren informieren, wann und wie wir Sprechstunden anbieten.

## TOP 5: Sonstiges

Sitzungen in vorlesungsfreien Zeit:

evtl. 22.02.2023 13:00 Uhr online / Präsenz

08.03.2023 13:00 Uhr online / Präsenz

und danach vll. noch eine

Robin schreibt nächste Sitzung Protokoll

---

#### Sitzungsende

14:25 Uhr
