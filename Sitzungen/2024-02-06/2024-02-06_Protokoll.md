# AK-MIT Sitzungsprotokoll 06.02.2024

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Carl Bodmann

#### Anwesende

[comment]: # (AK-MITler)

Carl, Robin, Magdalena, Lorenz

 

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

Theresa, Jan-Eric

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena


#### Sitzungsbeginn

13:05

---

## TOP 0: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

## TOP 1: Berichte

#### FS-MACH/CIW

- GEBKOM: Es fehlen Gelder für die Tutorien, Workshops... Wie kann man zusätzliche Gelder bekommen? - verschiedene Lösungen/Ideen stehen im Raum
        - andere Fakultäten mal anfragen, vorallem bei ETIT
        - erstmal mit Frau Beckmann reden
        - Infos bei der Mach Fakulät einholen
--> auf die FÜS, um mit den ETITs 

#### FS-ETIT


#### Weitere Berichte


## TOP 2: Feedback WahlInfo


## TOP 3: Website

Robin will wissen, was bei uns auf die Webseite muss und dann soll die Webseite aktuell gehalten werden. O-Phasenkalender, Sitzungstermine und evtl. sollen drauf.

- verlinken auf die anderen Webseiten für Klausurtermine (MACH, ETIT) --> am besten sollen die Leute im Campus suchen
- keine Klausurtermine
- andere Handreichungen auf die anderen Webseiten verlinken(FAQ, Anträge...)
- nur grundsätzliche Infos auf der Webseite



## TOP 4: Stuko MIT / Master SPO

- Master soll zum SS2025
- Ausarbeitung der SPO
- Auswahl, ob größere/ kleinere Vertiefungsrichtungen... -->großer Arbeitsaufwand evtl im CAS
- wie genau die Vertiefungsrichtungen aufgebaut sind, soll nochmal in der Stuko genauer kommuinziert werden; kam nicht überall richtig an, was Ziel ist mit dem Vorschlag
- jeweils ein Prof von ETIT und Mach für jede Vertiefungsrichtung (später dann auch für Anerkennung, Ausarbeitung)
    	- Treffen mit Doppelbauer, um mit ihm zu reden darüber
- (mit Kolben-Koch nochmal das Gespräch suchen)

## TOP 5: FÜS

- Vorstellung der neuen Master-SPO
- aktueller Stand bei ETIT einholen
- O-Phase bequatschen (gemeinsame Aktionen)
- Wahlen



## TOP 6: Sonstiges

Erstis einladen zu den Stammtischen MACH
Evtl. nochmal was zu Teambuilding

---

#### Sitzungsende

13:45


