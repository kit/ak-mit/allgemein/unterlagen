# AK-MIT Sitzungsprotokoll 13.09.2023

[comment]: # Es handelt sich um ein Gedächtnisprotokoll

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

Max Zuidberg
Carla Schneider
Thorben Bruns
Robin Helbig


[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

Theresa Streib

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Robin Helbig

#### Sitzungsbeginn

17:06 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Es gibt keine Protokolle zur Annahme.

## TOP 2: Berichte

#### FS-MACH/CIW

Thorben berichtet von den Problemen rund um die MKL I/II Klausur, bei der die Studis kürzer schreiben durften als im Modulhandbuch steht und erwartet war. Auch Nachschreiber mussten diese kürzere Prüfung schreiben. 

Theresa berichtet, dass es ein Treffen von der Fachschaft MACH/CIW mit dem IPEK gegeben hat, in dem das IPEK einige Fehler eingestanden hat.

#### FS-ETIT

Max berichtet von der Bufata im Oktober, zu der er wahrscheinlich mitgehen wird.

#### Erstsemesterbefragung

Max berichtet von dem Treffen mit Jasmin Azak zu den Ergebnissen der Erstsemesterbefragung.

## TOP 3: O-Phase


#### Nimm's MIT

Robin bestätigt, dass das NimmsMIT an die Fachschaft MACH/CIW geschickt wurde in einer finalen Version.

#### Allgemein

Prof. Doppelbauer wird im Rahmen der Infoveranstaltung in der O-Phase den Erstsemester am Anfang einen Einblick in seine Arbeit geben und sie für das Studium motivieren. Ein Treffen mit Robin und Prof. Doppelbauer ist für nächste Woche Mittwoch angesetzt.

Carla informiert die Sitzung über den aktuellen Stand mit den Genehmigungen bzgl. des Kennlerngrillens.

#### Schichtenliste

Carla regt an, dass sich die AKMITler in der Verantwortung sehen sollten, das MIT Kennlerngrillen zu ermöglichen indem sie als Helfer dabei sind.

#### Empfehlungen für Erstsemester

Die Sitzung einigt sich darauf, dass mit der neuen SPO und den Unklarheiten ein einheitliches Auftreten gegenüber den Erstsemestern wichtig ist und dass nicht empfohlen werden soll, Automatisierungstechnik ins erste Semester vorzuziehen.

## TOP 4: Infoveranstaltung

Max weißt darauf hin, dass die Infoveranstaltung für die Wahlfächer ab jetzt im ersten Semester gehalten werden muss, da bereits im zweiten Semester gewählt werden kann.

Thorben merkt an, dass bei der Survival ETIT Veranstaltung im ersten Semester einige Probleme in der Kommunikation dazu geführt haben, dass MIT nicht ausreichend abgedeckt wurde und schlägt vor eine eigene zu machen.

Es muss drauf geachtet werden, dass in Zukunft beide SPOs berücksichtigt werden und es potentiell verschiedene Infoveranstaltungen geben muss.

## Top 5: Kommunikation mit den Fachschaften

Max bringt auf, dass es nach der letzten FüS einige Gespräche über die Probleme in der Kommunikation mit den Fachschaften gab
und möchte die Meinung der Sitzung hören.

Die Sitzung führt verschiedene Problemstellen auf, bei denen es zu Missverständnissen und Konflikten gekommen ist und diskutiert diese.
Alle sind sich einig, dass dies ein wichtiges Thema ist und Handlungsbedarf herrscht.

Es folgt eine lange, offene Disskusion über die vergangenen Abläufe und Probleme.

## Top 6: Sonstiges

Der Termin der nächsten Sitzung ist noch offen.

#### Sitzungsende

18:49 Uhr
