# AK-MIT Sitzungsprotokoll 19.07.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

Carla Schneider, Thorben Bruns, Johann Fox, Max Zuidberg

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

Lorenz Neumann

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Johann Fox

#### Sitzungsbeginn

17:02 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg begrüßt alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Annahme von Protokollen wird aufgrund mangelnder Korrekturen vertagt. 

## TOP 2: Berichte

#### FS-MACH/CIW

Das Sommerfest hat stattgefunden. Die Tour de fsk hat stattgefunden am Montag. Lorenz berichtet von der StuKo MACH/CIW. Die StuKo plant die neue SPO MACH Master bereits im kommenden Wintersemester umzusetzen. Damit passt sich die Fakultät MACH dem Zeitplan der Fakultät ETIT an. Die Sitzung begrüßt diesen Zeitplan. Die Abhängigkeiten von MACH und ETIT sind für MIT zum Glück nicht so gravierend wie im Bachelor.
Ein Vorschlag wäre es die Änderungen in einer geeigneten Art und Weise im Internet vorzustellen.

#### FS-ETIT

Nein, hier gibt es auch nichts zu berichten. Am 27. Oktober 2023 wird es einen Workshop im HaDiKo der pbs geben zum Thema "Studierende in Not – erste Hilfe bei psychischen Schwierigkeiten". Bei Interesse bitte an Max wenden.
Die Skom ETIT hat stattgefunden. Max war anwesend. Es wird erwartet, dass auf der Sitzung in der nächsten Wochen ausführlicher berichtet werden kann.

#### O-Phase

Das Dekanat MACH hat angefragt, inwiefern wir uns an den Informationen für die neuen Erstsemesterstudierenden beteiligen wollen.

**Todo:**

- Carla und Robin setzen sich mit der überarbeiteten Version der "Nimm's MIT" auseinander. Lorenz hat Bereitschaft signalisiert hier mithelfen zu lassen.

## TOP 3: Ämter

Am kommenden Montag, den 24.07.2023 findet die nächste Fachschaftsübergreifende Situng statt. Bis dahin sollten für alle zu besetzenden Ämter Vorschläge feststehen.

Für die folgenden Ämter stehen die folgenden Kandidaturen fest:

##### Leitung

- Carl
- **vakant** möglicherweise Cyril oder Feline

##### Studienkommission

- Robin
- Carla
- Carl
- Lorenz

##### Prüfungsausschuss

- Ellen
- Magdalena
- Carl
- Thorben
- Leo

##### Master Zulassungskommission

- Magdalena
- Carl
- Feline (möglicherweise)

Allgemein bleibt festzustellen, dass der AK MIT mehr Aktive braucht. Carl kann nicht alle Posten wahrnehmen und die anderen sind auch gut beschäftigt. Die Aufgaben sollten auf mehr Schultern verteilt werden.

#### Master Zulassungskommission

Hier fand wohl der erste Termin statt, allerdings ist niemand der gewählten studentischen Vertreter zu diesem Termin aufgetaucht. Das ist natürlich schade. Hier sollten im Interesse der Studierenden auch Studierende auftauchen.
Hier ist eine Rückmeldung seitens des AK MIT erforderlich. Am nächsten Freitag, den 28.07.2023 findet die nächste ZuLaKo Master statt. Hier sollten studentische Vertreter anwesend sein.
Im Rahmen der nächsten FüS könnte nochmal das Thema Bezahlung der Arbeit der Studies in der Zulassungskommission diskutiert werden. Wenn dieses Amt aus dem Ehrenamtsbereich herausgenommen wird, könnten sich dafür mehr Interessierte finden.

Ergebnis der Diskussion: Das Thema soll auf der nächsten FüS diskutiert werden (Aufwandsentschädigung, HIWI-Vertrag, weiterhin Ehremant, etc.).

**ToDo:**

- Thorben wird hierzu Folien vorbereiten und sie am Montag vor der FüS mit Max abstimmen.

## TOP 4: Neue SPO

#### Praktikumsordnung

Prof. Geimer hat die aktualisierte Datei mit den Praktikumsrichtlinien für das Berufspraktikum Bachelor in den Sharepoint hochgeladen. Die wesentlichen Änderungen sind die folgenden:

- Berufspraktika in Hochschuleinrichtungen sind ausgeschlossen.
- Die Unterlagen können per E-Mail als pdf-Datei von der studentischen KIT-E-Mailadresse an das Praktikantenamt geschickt oder in Papierform in den Brief-kasten des Praktikantenamts eingeworfen oder per Post gesendet werden. Eine persönlichen Abgabe ist ebenfalls während der Sprechzeiten des Praktikantenamts möglich. Die Unterlagen müssen spätestens sechs Monate nach Beendigung des Praktikums eingereicht worden sein.

Für die StuKo ist das folgende zu besprechen:

- Wie wird bezüglich der sechs Monats Frist in Bezug auf geteilte Praktika umgegangen? Beginnt der Countdown nach den 13 Wochen oder nach 26 Wochen?
- Wie wird abgeprüft, ob in beiden Bereichen, wie vorgeschrieben, gearbeitet wurde?

#### Wahlbereich Bachelor MIT

Hierfür wird ein neuer Termin anberaumt.

##### Vertiefungsbereich MACH

- Zusammenlegen von Ströle und Thermo als ein Block
- MKL B und C
- Werkstoffkunde (von MACH)
- Fertigungstechnik (Veranstaltungen vom wbk)

## TOP 5: Sonstiges


#### Webseite

Die Betreuung der Webseite wird Robin übernehmen. Die Sitzung dankt Dir dafür sehr herzlich!

**ToDo:**

- Johann leitet die Mail von Paul Schäfer an Robin weiter.
- Robin wird die Links auf der Fachschaftsseite von MACH/CIW und ETIT auf Aktualität kontrollieren.

#### Fachschaftsübergreifende Sitzung

Termin: 24.07.2023 19:00, Geb. 10.91, R380. 


**Nächste Sitzung am Mittwoch, 26.07.2023 17:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

18:10 Uhr
