# AK-MIT Sitzungsprotokoll 16.01.2024

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Carl Bodmann

#### Anwesende

[comment]: # (AK-MITler)

Carl, Magdalena, Lorenz, Carla, Fabian, Matthias, Jan

 

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

Jan-Eric, Theresa

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena

#### Sitzungsbeginn

13:05

---

## TOP 0: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

## TOP 1: Berichte

#### FS-MACH/CIW

- wollen das die SPOs parallel kommen, aber mal abwarten

#### FS-ETIT

-

#### Weitere Berichte


## TOP 2: FÜS

- Themen siehe Folien
- Masterzulako wurde nicht gewählt (gab Ungereimtheiten auf der letzen FÜS, warum diese nicht gewählt wurden) und braucht auch Leute
    - die Leute werden rückwirkend gewählt
- bei weiteren Anmerkungen oder Themen an Carl


## TOP 3: Stuko MIT/ Master SPO

- Plan der neuen Master-SPO in den Folien
    - freier Wahlblock mit mehr ECTS für z.B. ein Erasmus-Semester
- Elektrotechnik und MAschienenbau sollen kommendes Semester kommen
- Vertiefungsfächer bleiben gleich zur letzen SPO
- Aus dem Bachelor kann man alle drei Master(E-technik, Maschienenbau und Mechatronik) wählen
- Technikethik wird noch gesucht, wie man das genau umsetzen könnte (welches Institut/Dozent?)
    - evtl. Arbeitsfeld des Ingenieurs erweitern (Prof. Geimer)
- man kann extern bei der Info-Fakultät schreiben ohne zusätzlichen Betreuern, deswegen soll der Masterblock gleich bleiben


- Treffen mit Geimer
    - Methodisch und Fachlich- Trennung
    - 24h-Workshop soll abgewartet werden

- nach dem grundsätzlichen beschließen wird die SPO weiter ausgearbeitet

- technische Ethik / ARRTI
    - in den Möglichkeiten leider etwas eingeschränkt
    - können für den Master das nicht unbedingt garantieren, das die Kurse unterschiedlich sind
    - Erweiterung von technische Ethik auf Verantwortung, Nachhaltigkeit...
    - andere Möglichkeiten siehe Folien, evtl. Modulhandbuchänderungen jedes Semester


## TOP 4: Wahlinformationsveranstaltung MIT

- 30.01. 15:45-17:15Uhr
- grundsätzliche Infos in den Folien
- Wahlinfos und Fächervorstellung
- Überarbeitung der Folien
- danach sollen ältere Studierenden am besten für Fragen zur Verfügung stehen



## TOP 5: MSuP-Anschlussveranstaltung

- grundsätzliche Infos in den Folien
- wer bestellt die Pizza?
- Kühlschrank für Bier und kaltstellen
- Glühwein?

## TOP 6: Sonstiges

- kommende Termine stehen in den Folien


---

#### Sitzungsende

13:40Uhr


