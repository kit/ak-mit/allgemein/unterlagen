# AK-MIT Sitzungsprotokoll 07.06.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Feline Pirchmoser, Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

Magdalena Janocha, Carla Schneider, Johann Fox, Robin Helbig (verspätet)

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

Theresa Streib (ab 17:21 Uhr anwesend, TOP 4)

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Johann Fox

#### Sitzungsbeginn

17:07 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung nicht ordnungsgemäß eingeladen wurde und die Sitzung daher nicht beschlussfähig ist. Meinungsbilder sind trotzdem zulässig.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Protokoll der letzten Sitzung wurde weder korrigiert, noch kann es angenommen werden.



## TOP 2: Berichte

#### FS-MACH/CIW

Die Fachschaft MACH/CIW fährt am kommenden Wochenende auf Hütte.

#### FS-ETIT

Vor oder nach der Sitzung gibt es die Möglichkeit, Bilder für die Fotowand in der ETIT-Fachschaft zu machen.

## TOP 3: O-Phase

#Top wird auf später verschoben, nach TOP 4.

Carla weiß von Lorenz, dass sich die O-Orga seitens der beiden Fachschaften schon zusammen gesetzt haben. Ein Plan seitens MACH steht wohl schon.

Die neuen Mechatronik-Studies sollen an der Spaß-Einführungsveranstaltung von MACH teilnehmen. Danach geht es in den Schlossgarten und abends gibt es grillen bei den ETITs.

Aktuell gibt es die Idee, für die MITler einen Plan zu erstellen, der den neuen Studies ausgewählte Veranstaltungen empfiehlt.

Es wird im Moment noch überlegt, welche anderen MIT-spezifischen Veranstaltungen angeboten werden.

Laut Theresa ist der Plan, die O-Partys für MACH/CIW und ETIT zusammen zu veranstalten. Das wäre perfekt für MIT. Die Hauptorga für die Party wird seitens MACH/CIW gestellt. Die Orga ETIT-seitig übernehmen Tilman, Marco.

Robin ist zwar nicht auf der Sitzung, ist aber willig, die Orga MIT-seitig zu übernehmen.

## TOP 4: 10-Tage-Öffnungszeit

Zum 10-jährigen Bestehen der verfasste Studierenschaft wird die Fachschaft ETIT eine 10-tägige Öffnungszeit veranstalten.
Das Event findet statt von Montag, den 26.06.2023 9:00 Uhr bis Mittwoch, 05.07.2023, 23:59 Uhr.

Teile des AK MIT möchten einen dieser Tage übernehmen und den Tag Mechatronik-lästig zu gestalten. Geplant ist das von Donnerstag, den 29.06. 10:00 Uhr bis Freitag, den 30.06. 10:00 Uhr zu tun.
Um die 24 Stunden zu füllen wird noch nach entsprechenden Aktivitäten gesucht.

Magdalena und Max haben die folgendnen Ideen entwickelt:
- Der Morgen soll mit einem "Repair-Cafe" gestartet werden. Werkzeug sei vorhanden.
- Für den ganzen Tag sollen alle Mechatronik-Studierendnen des KIT eingeladen werden. So sollen sie an die Fachschaft gebracht werden.
- Von 12:00 bis 14:00 Uhr soll gemeinsam gekocht werden.
- An diesem Tag sollen als ein Projekt T-Shirts für den AK MIT gestaltet werden.
- Beerpong vs. Matthiesen, der Tag muss noch geklärt werden.
- Max möchte eine Segway-Tour im Sonnenaufgang machen.
- Max möchte mit einer Laserharfe Musik machen.
- Die Frage der Finanzierung ist noch nicht geklärt, merkt Feline an. Max schläft hierbei vor auch bei MACH/CIW nach Geld zu fragen. Theresa empfiehlt hierfür einen sehr detaillierten Haushaltsplan vorzulegen, damit das Ganze von der FS MACH/CIW Sitzung beschlossen wird. Max und Feline setzen sich deswegen bis nächste Woche zusammen um nächste Woche das auf die Sitzung MACH/CIW zu bringen.
- Am Abend soll eine FüS stattfinden. FüS steht an dieser Stelle allerdings für Fachschaft übergreifendes Saufen.
- Weitere Ideen sind sehr willkommen.

## TOP 5: Sonstiges

Die nächste FüS soll gegen Ende Juli stattfinden. In diesem Rahmen sollen die folgenden Ämter gewählt werden: PA, StuKo-Mitglieder und AK MIT-Leitung.

Im Rahmen der heutigen FüS soll eine Satzungsänderung vorgeschlagen werden, in dessen Rahmen die Einladungsfrist von zehn auf fünf Tage reduuiert wird.

ToDo: Die gesante Satzung FüS soll auf Unstimmigkeiten überprüft werden und bis zur nächsten FüS überarbeitet werden.

## TOP 6: StuKo MIT

Max und Johann möchten im Rahmen der nächsten StuKo gerne anmerken, dass die Erstellung der aktuellen SPO nicht gut gelaufen ist. Die Frage ist nur wie.

- Johann schlägt vor, das rhetorisch-selbstironisch zu tun.
- Feline möchte gerne ein Feedback-Gespräch dazu.

Kompromiss: Soll in der nächsten StuKo angesprochen werden mit dem Angebot eines klärenden Gespärchs, sofern das seitens der Studiendekane gewünscht ist. In diesem Gespräch soll, sofern es zustande kommt, geklärt oder überlegt werden, wie MIT in Zukunft weniger stiefmütterlich behandelt werden könnte.

Der Punkt "StuKo-Feedback" soll auf die Tagesordnung der nächsten StuKo aufgenommen werden.

## TOP 7: Sonstiges

Auf der MACH/CIW-Hütte wird Carla einen Workshop zum Thema "Kommunikation mit dem AK MIT" machen. Die Ergebnisse stellt sie in der nächsten Woche vor.

**Nächste Sitzung am Mittwoch, 14.07.2023 17:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

17:55 Uhr
