# AK-MIT Sitzungsprotokoll 16.11.2022

#### Sitzungsleitung

Feline Pirchmoser, Max Zuidberg

#### Anwesende

Max Zuidberg, Feline Pirchmoser, Carl Bodmann, Magdalena Janocha, Thorben Bruhns, Robin Helbig, Marco

#### Protokollierende Person

Thorben Bruns

#### Sitzungsbeginn

13:00 Uhr

---

## TOP 0: TO-Annahme

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 


## TOP 1: Protokoll

Das Protokoll der letzten Sitzung vom 28.10.2022, geschrieben von Carl Bodmann wurde angenommen.


## TOP 2: Berichte

#### FS MACH/CIW:

Am 09.12.2022 findet das Winterfest statt. Es werden noch Helfende gesucht. 

#### FS ETIT:

Am 18.11.2022 findet die Nacht der Wissenschaft statt. es werden noch Helfende gesucht. 

Heute Abend findet das Meet the Fachschaft statt. 

#### TOP 3: Stuko Treffen mit Prof. Hiller

Am Dienstag, 15.11.2022 haben sich die stud. Mitglieder der Stuko MIT und ETIT mit Prof. Hiller getroffen um den Vorschlag für eine neue SPO ETIT zu besprechen. 

- Anders als vorherige SPO Änderungen fällt diese recht umfangreich aus. 
- Idee: Bologna Gedanken besser umsetzen und den Bachelor unabhängiger vom Master machen. Wurde erfolgreich an anderen Unis umgesetzt.
	- Pflichtbereich auf das nötigste reduzieren, was wirklich alle benötigen
		- Viele Fächer werden in einen ersten und zweiten Teil aufgespalten; der erste Teil enthält verpflichtende Grundlagen für alle, der zweite Teil ist optional, bzw. Teil der jeweiligen Vertiefungsrichtung. 
		- Teilweise werden die Grundlagenteile von ähnlichen Fächern zusammengelegt. 
	- Vertiefungsrichtungen ähnlich dem Master einführen
	- Insgesamt mehr Wahlfreiheit für Studierende
- Mobilitätsfenster (Zeitraum für ein Auslandsemester) ermöglichen
- Steigerung der Attraktivität insgesamt

Wegen dieser Änderungen und weil MACH ebenfalls umfangreiche Überarbeitungen plant, wollen die stud. Mitglieder der Stuko auch eine neue SPO für MIT ausarbeiten. 

Ideen und Verbesserungsvorschläge für den Studiengang MIT an die Stukomitglieder richten (persönlich oder an stuko@lists.ak-mit.vs.kit.edu). 

## TOP 4: AK Moderne Klausuren

Aus Zeitgründen auf die nächste Sitzung vertagt. 

## TOP 5: MIT-Sprechstunde

#### Reduzierung der Termine

Da die Sprechstunde wenig besucht wird, wurde vorgeschlagen, sie nur alle zwei Wochen abzuhalten. 

Dagegen spricht, dass die Sprechstunden den Austausch mit der FS-ETIT fördert und die Zeit für (Gremien)Arbeit gut genutzt werden kann. Weiterhin ist ein wöchentlicher Regeltermin leichter zu kommunizieren als wenn die Sprechstunde nur an bestimmten Tagen stattfindet. 

Der Vorschalg wird verworfen.

#### Termine

Die Sprechstunden finden weiterhin jede Woche freitags um 12:30-13:30 Uhr in der FS ETIT statt. 

Besetzung der nächsten Sprechstunden:

- 25.11.2022: Carl, Max
- 02.12.2022: Carl, Magdalena, Max, Feline
- 09.12.2022: Magdalena, Max
- 16.12.2022: Carl, Max, Feline

#### Werbung

Die Sprechstunde wurde mit der letzten Sitzungsmail beworben, ob die Info als Teil der Einladung viele Studierende erreicht hat, ist allerdings fraglich. 

Die Sprechstunde soll deswegen erneut mit einer eigenen Mail an alle MITler*innen beworben werden. Die Webseite soll ebenfalls darauf aufmerksam machen. 

## TOP 6: Weihnachtsfeier Erstis

Der AK-MIT organisiert jedes Jahr eine Weihnachtsfeier für die Erstis. Es gibt Glühwein und es werden Kontakte zwischen dem AK-MIT und dem Jahrgang geknüpft. 

#### Orga

Es wird eine Person für die Orga gesucht. Robin meldet sich für die Aufgabe. 

Traditionell findet die Feier im ETIT Fachschaftsraum am IBT statt. 

Es muss ein Termin gefunden, sowie Glühwein organisiert werden. 

Der Termin soll auf der nächsten Sitzung festgelegt werden. 

#### Ausweitung auf alle Semestern

Wegen geringem Andrang in den letzten (Corona) Jahren wurde vorgeschlagen, die Feier auf alle MIT Jahrgänge auszuweiten. 

Ein Vorteil wäre die bessere Vernetzung zwischen den Jahrgängen. 

Das Problem ist, dass für zu viele Leute geplant werden muss; aus 20-40 würden 200-400 werden. Damit steigen Aufwand und Kosten unverhältnismäßig. 

Der Vorschlag wird verworfen. 

## TOP 7: Survial Veranstaltung

Für ETIT und MEDTec wird eine Veranstaltung organisiert, die die wichtigsten Punkte zum Studium erklären (Fristen, Ansprechpartner, ...). Ansprechpartner für die Organisation ist Frau Jasmin Azak (ETIT Dekanat).

Es wurde angeboten, auch den Studiengang MIT aufzunehmen. 

In der Vergangenheit ist bei solchen gemischten Veranstaltungen Verwirrung aufgekommen, welche Infos (auch) für MIT gelten und welche nicht. Der AK-MIT beführwortet eine gemeinsame Veranstaltung daher unter der Bedingung, dass die Infos eindeutig nach Studiengang (MIT, ETIT, MEDTec) getrennt sind. 

Im vergangen Jahr war der Andrang eher gering. 

Genauer Termin steht noch nicht fest, soll jedoch im Januar sein. 

Vorschlag: Im Anschluss an das Treffen ein Get Together organisieren. 

Am Montag, 21.11.2022 treffen sich die stud. Vertreter der Stuko MIT mit Frau Azak und nehmen die o.g. Vorschläge mit. 

## TOP 8: Sonstiges, nächste Sitzung

Werbung in Erstigruppe: sofort löschen

**Nächste Sitzung am Mittwoch, 23.11.2022 13:00 im ETI Seminarraum 016 (Geb. 11.10)**

---

#### Sitzungsende

14:10 Uhr
