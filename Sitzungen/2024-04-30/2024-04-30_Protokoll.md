# AK-MIT Sitzungsprotokoll 30.04.2024

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Carla und Carl

#### Anwesende

[comment]: # (AK-MITler)

Max B., Carla, Carl, Matthias, Simon, Fabian, Marco, Johannes, Julius, Valentin, Simon, Lorenz, Thorben, Moritz
 

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)
Carla 

#### Sitzungsbeginn
13:00

---

## TOP 0: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

## TOP 1: Berichte

#### FS-MACH/CIW
Bierinsel mit ETIT geplant, Stattfinden ist wetterabhängig

#### FS-ETIT
x

#### Weitere Berichte
Technik Ethik 
- Iliaskurs hat keinen inhaltlichen Mehrwert, Test auch ohne machbar
- Prüfung chaotisch, 20% Durchfallquote
- Ergebniseintragung /-änderung konnte z.T. live eingesehen werden 
- Ergebnisbekanntgabe nicht für alle gleichzeitig
- Studis wollen sich erkundigen, was passiert, wenn man bisher keine Mail bekommen hat 

## TOP 2: Stuko MIT
- Bachelor 2016.1: Durch Änderung des Bachelors 2023 verändern sich LP (minus 1). Beschluss der Stuko: LP behalten aus Bestandsschutzgründen
- Bachelor Auswahlkommission: Plätze auf 150 angehoben; Kommission tagt ab 450 Bewerbungen 
- Master Doppelabschluss mit INSA Lyon: Zuspruch aber Ablaufplan muss noch ausgearbeitet werden
- Master SoSe 2025: Planungsstand darf kommuniziert werden 
- Medizintechnik VR im Master: ETIT schafft die VR ab und ersätzt ihn durch den Master MEDT. Für MIT wollen wir die VR behalten oder die Möglichkeiten entsprechend in MEDT abbilden (dagegen spricht, dass Kapazität begrenzt, komplette Breite von MEDT BA Absolventen nicht verstehbar, man will das nur als Nebenfach)

## TOP 3: Gremien
Erklären der Ämter:
- BA Auswahlkommission
- Studienkommission 
- Prüfungsausschuss
- Master Zugangskommission 

## TOP 4: O-Phase

## TOP 5: FÜS
vertagt

## TOP 6: Website
Gemeinsame Terminübersicht für Klausuren hilfreich 
In O-Phase gemeinsamer Kalender hilfreich oder beide Kalender als ical zum Runterladen 

## TOP 7: Sonstiges
Nächste Termine: 
- 02.05. Bierinsel 
- 03.05. AK MIT Get togehter 

---

#### Sitzungsende
13:53



