# AK-MIT Sitzungsprotokoll 30.12.2022

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # 

Max und Feline


#### Anwesende

[comment]: # (AK-MITler)

Max, Feline, Leo, Thorben, Vanessa, Robin, Marianne, Carl, Simon, Cyril, Magdalena, Marianne, Marco



[comment]: # (FS-MACH/CIW) 

Sandra (FS-MACH/CIW), Jil (FS-MACH/CIW)



[comment]: # (FS-ETIT)

Jean-Pierre (FS-ETIT)



#### Protokollierende Person

Magdalena



#### Sitzungsbeginn

13:02 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Das Protokoll der letzten Sitzung vom 23.11.2022, geschrieben von Magdalena wurde noch nicht angenommen, wird noch rumgeschickt.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Das Protokoll der letzten Sitzung vom 23.11.2022, geschrieben von Magdalena wurde noch nicht angenommen, wird noch rumgeschickt.

## TOP 2: Berichte

#### FS-MACH/CIW

Am 9.12. findet das Winterfest statt. Bitte in die Helferliste eintragen.


#### FS-ETIT

Es kommen diesen Freitag aus ganz Süddeutschland Fachschaftler nach Karlsruhe. Einladung dort dabei zu sein. (Infos: Jean-Pierre)

Einladung zur Tour der FSK
Voschlag, das der AK MIT dort auch mit macht, entweder bei der FS MACH/CIW (Ansprechperson: Cyril) oder bei der FS Etit (Ansprechpartner: Jean-Pierre)


## TOP 3: Prüfungsausschuss

neuer Professor und neuer wissenschaftlicher Mitarbeiter;
Ausschuss läuft grundsätzlich mal


## TOP 4: Studienkommission

Vorstellung über neue SPO-Rahmenbedingungen über die mündliche Nachprüfung

Es soll nun möglich sein, bei der mündlichen Nachprüfung auch die schriftlichen Leistungen mit einzubeziehen. Zudem soll es kein Recht auf eine mündliche Nachprüfung geben, wenn es bei dem zweiten Versuch zu einem Täuschungsversuch kommt.
Es wird versucht, dies im Fakrat Etit, sowie Mach, zu verhindern. Bei einem Täuschungsversuch soll es, wie bisher zu einer Einzelfallentscheidung durch den PA kommen.

Vorlesung kognitive Systeme wird von Nachfolgervorlesung abgelöst.
Abwarten, wie Etit das Fach in der neuen SPO integriert(entweder Bachelor oder Master)und dann bei MIT anpassen. Hintergrund: Bisher ist die Vorlesung im Master und Bachelor möglich. 

## TOP 5: neue SPO MIT

neue SPO MACH:

* der neue geplante Studienplan wird noch geschickt

* relevante Änderungen für MIT sind die Umstellung von TM und MKL

* Probleme vorallem im ersten Semester MIT

* grundsätzlich steht die SPO MACH

* letze Stuko MACH hat leider nicht statt gefunden


Besprechung für die neue SPO mit Etit und MACH soll stattfinden.

neue SPO MIT:

- es gibt kein Eingnungstest für die Zulassung für den Mechatronik Master
- sonstiges: siehe Folien



## TOP 6: Sonstiges



**Nächste Sitzung am Mittwoch, 07.12.2022 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:55 Uhr
