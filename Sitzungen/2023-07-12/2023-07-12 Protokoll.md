# AK-MIT Sitzungsprotokoll 12.07.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

Carla Schneider, Max Zuidberg

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Max Zuidberg

#### Sitzungsbeginn

17:20 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg begrüßt ~~alle Anwesenden~~ Carla Schneider. Die Sitzung ist nicht beschlussfähig.

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Protokolle existieren. 

## TOP 2: Berichte

#### FS-MACH/CIW

Es war 24-Stunden-Workshop der Fakultät MACH. Man möchte die neue Master-SPO zum WS 24/25 einführen - gleich wie ETIT (nach aktuellem Stand). Bisher ist es nur eine Absicht, das kann sich noch ändern. Thorben kann vermutlich auf der nächsten Sitzung mehr berichten. 

Es ist aufgefallen, dass im FS ETIT Wahl-Funke auf der Seite zur Höchststudiendauer der Studiengang MIT fehlt. 

#### FS-ETIT

Es wurde berichtet, dass es einen geplanten Workshop "Studierende in Not - Erste Hilfe bei psychischen Schwierigkeiten" der PBS am 27.10.2023 im HADIKO geben wird, in dem man geschult werden kann wie wir mit psychischen Problemen Studierender umgehen können.

Carla merkt an, dass das insbesondere für die PA Mitglieder interessant ist. 

#### ARRTI Treffen

Max berichtet. Das ARRTI, welches die Veranstaltungen für das neue Technikethik-Pflichtmodul anbietet, hat sich nochmal mit dem AK MIT getroffen. Vom AK-MIT anwesend waren Feline, Cyril und Max. Feline und Cyril haben ggf. noch Notizen mit mehr Details zum Gespräch. 
Besprochen wurden folgende Punkte:

- Angebot des ARRTIs, Vorstellung der einzelnen Veranstaltungen (Beschreibung der einzelnen Veranstaltungen siehe MHB, Webseite, ...).
	- Andere Angebote und Formate sind nicht pauschal ausgeschlossen, erfordern aber mehr Arbeit. Vorerst scheint es sinnvoller, die vorhandenen Angebote zu nutzen und aus den Erfahrungen damit zu lernen. 
	- Die meisten Angebote sind eher klein und passen gut in die verfügbaren 2LP
	- 2LP reichen nicht für eine umfangreiche Behandlung des Themas. Da das Angebot sehr modular ist, können aufbauende Module bei Interesse in die ÜQs gewählt werden. 
	- Manche Veranstaltungen richten sich explizit an Studierende, die bereits Ethik-Vorlesungen gehört haben. Diese sind nicht als Einstieg geeignet. 
		- **Todo**: Studierende in der zukünftigen Info-Veranstaltung im ersten Semester über das Angebot und explizit auch diesen Punkt informieren. 
		- **Todo**: ARRTI bei (der Vorbereitung) dieser Info-Veranstaltung einbinden.
	- Teils sind die Veranstaltungen bewertet, teils nicht. Das ARRTI hat nicht direkt eine Präferenz, merkt aber an, dass das Festlegen einer spezifischen Note ggf. mehr Arbeitsaufwand bedeutet. 
		- **Todo**: Überlegen was uns lieber ist. 
- Möglichkeit, Co-Teaching mit Profs bestehender Vorlesungen zu machen. Sehr sinnvoll, aber auch hier nur sehr begrenzte Kapazitäten. 
	- **Todo**: Überlegen bei welchen Vorlesungen/Profs/Institute geeignet wären. 
- Das ARRTI hat derzeit nur sehr begrenzte Ressourcen. Eigentlich kann man nur dank des Ilias-Kurses den zu erwartenden Ansturm stemmen. **Todo**: Gerade wenn das Angebot ausgebaut werden soll und die Veranstaltungen auch in andere Studiengänge aufgenommen werden soll, wäre es wichtig, an den entsprechenden Stellen auch von unserer Seite aus immer wieder anzumerken, dass dort Mittel fehlen (werden). 
- **Todo**: Es gibt einige Punkte, die der AK-MIT klären muss. Anschließend soll ein weiterer Termin mit dem ARRTI ausgemacht werden. 
 
## TOP 3: Master SPO

### Seamless Engineering

Seamless Engineering: Will man das nach wie vor als Pflicht-Workshop einführen? Gibt es Feedback von Teilnehmer*innen? Wie sehen die Teilnehmer*innen das?

Max: im letzten Durchlauf (WS22/23) war das Praktikum noch nicht reif um ein Pflichtpraktikum. Deren Zeitplan sah vor noch min. ein weiteres Jahr Probelauf zu machen bevor es verpflichtend werden wird. 

## TOP 4: O-Phase

Carla hat sich zwar bereit erklärt, an der Orga der MIT Seite mitzuwirken, hat aber nicht die Kapazität alle aktuell anfallenden Aufgaben zu übernehmen. Insbesondere die Leitfäden zu überlesen wäre eine Aufgabe, die auch andere übernehmen könnten:

- **Nimm's MIT**. Höchste Priorität!
- Bachelor Leitfaden
	- Wird (theoretisch) von der FS MACH/CIW gepflegt
- Master Leitfaden
	- Wird (theoretisch) von der FS ETIT gepflegt

Alle müssten zumindest mal korrekturgelesen werden. 

Offene Frage: Welche Sponsoring-Angebote werden in die MIT Infomaterialen übernommen? Die beider Fachschaften? Keine?



## TOP 5: Sonstiges

Carla bittet darum, dass die TOPs wenigstens ein paar Stunden im Voraus bekannt sind. 

**Nächste Sitzung am Mittwoch, XX.XX.XXXX 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

17:50 Uhr
