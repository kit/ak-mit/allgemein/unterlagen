# AK-MIT Sitzungsprotokoll 18.01.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Feline, Max

#### Anwesende

[comment]: # (AK-MITler)

Feline, Max, Simon, Thorben, Carl

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Carl

#### Sitzungsbeginn

13:04 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Die überarbeiteten Protokolle werden angenommen.


## TOP 2: Berichte

#### FS-MACH/CIW
Winterhütte:

MIT FAQs wurden aktualisiert, wird in Zukunft auf Website eingepflegt.
SPO Zeitplan von MACH nun bekannt
Awareness: Nachteilsausgleich von Fakultät zu Fakultät wohl unterschiedlich. 

Diskussion: 

Warum?
Welcher PA für MIT zuständig? Müsste eigentlich PA MIT sein, war in der Vergangenheit wohl nicht so.
Wann Einzelfallentscheidung, wann Pauschalentscheidung?
Allgemein sehr schwierig und mühselig Nachteilsausgleich anerkannt zu bekommen.

Stuko MACH:

Ganz viel Krimsgrams für SPO, Maximalstudiendauer soll von studentischer Seite erhöht werden, Profs. aktuell eher dagegen.
Auf 24h Workshop evtl. nicht nur Wahlbereich.


#### FS-ETIT

Fototermin heute 18 Uhr (vor der Sitzung), gerne auch AK-MITler, damit AK-MIT bei ETIT sichtbar.

## TOP 3: AK LAZ
Verantworlich: 

MIT: Magdalena und Robin
ETIT: Wendelin Karg
MACH: ?
laz@lists.ak-mit.vs.kit.edu
Ansprechpartner IPEK: Matthias Eisenmann


## TOP 4: Survival ETIT

Infoveranstaltung für ETIT, MIT, MedTec + get2gether bei Glühwein und Waffeln
Di: 24.01.2023: 15:45 - 17:15 im HSI

## TOP 5: Jahrgangstreffen MSuP

Bier damals von Matthiesen, alle Jahrgänge einladen.
Aktueller Stand: Robin ist nicht da

## TOP 6: FüS

Häufiger für Reden miteinander:
Idee für zukünftigen Umgang mit FÜS
O-Phasen Orga, Nachteilsausgleich Ideen und Vorschläge 
SPO: Onlineprüfung, Täuschungsversuche , ...

## TOP 7: Sonstiges

Email Adressen mit @lists.ak-mit.vs.kit.edu :
Nochmal beim Asta versuchen, verkürzte Adresse zu bekommen.

SPO Absatz zum PA: gemeinsamer Prüfungsausschuss -> 2 Studis aus Bachelor und Master. Evtl. ändern, da laut SPO zwei Masterstudierende im PA sitzen müssten. SPO dahingehend ändern.

Vorlage für Anfragen die an info@ kommen. (Magdalena kümmert sich drum)

**Nächste Sitzung am Mittwoch, 25.01.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

14:00 Uhr
