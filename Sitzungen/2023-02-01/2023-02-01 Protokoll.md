# AK-MIT Sitzungsprotokoll 01.02.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

XX

#### Anwesende

[comment]: # (AK-MITler)

Max, Manuel, Carl, Magdalena, Robin

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Carl

#### Sitzungsbeginn

13:15 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg begrüßt alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Annahme der Protokolle wird vertagt.

## TOP 2: Berichte

#### FS-MACH/CIW

nichts

#### FS-ETIT

Rundertisch der Nachhaltigkeit soll in Zukunft beratendes Gremium erweitern, an welchem jede Person teilnehmen kann.

KIT muss sparen.

## TOP 3: SPO

Stuko war am Montag (30.01.2022). Während 24h Workshop wurde mal wieder alles über den Haufen geworfen. MKL A soll z.B. 8 ECTS bekommen. Daraufhin übers WE Plan von Studierendenseite entworfen (siehe neuen SPO Plan). DT und IT zusammenlegen, technische Ethik, FuW nur Felder, Thermo Grundlagen für Mechatroniker. Professoren waren teilweise sehr positiv gestimmt zu unserem Vorschlag, teilweise sehr schockiert. Es wird bis zur nächsten StuKo versucht unseren Plan umzusetzen, Studierende und Professoren gehen auf die Professoren der Veranstaltungen zu.

FakRat ETIT und MACH haben bzgl. Onlinesatzung und Täuschungsversuch anders gestimmt, damit für MIT rechtens müssen beide FakRäte zustimmen.

## TOP 4: MSuP Jahrgangstreffen

Bekommen vom IPEK 6 Kästen Bier. Robin braucht Hilfe für Transport der Getränke. 


## TOP 5: Sonstiges

AK-MIT Treffen entspannt im Ox oder so, um einfach mal unabhängig der Uni mal zusammenzusitzen und Spaß zu haben. 
Magdalena kümmert sich drum. 



**Nächste Sitzung am Mittwoch, 08.02.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:57 Uhr
