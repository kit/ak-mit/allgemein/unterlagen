# AK-MIT Sitzungsprotokoll 28.05.2024

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Carl

#### Anwesende

[comment]: # (AK-MITler)

 

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)


#### Sitzungsbeginn


---

## TOP 0: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

## TOP 1: Berichte
#### aktueller Stand O-Phase

#### FS-MACH/CIW

#### FS-ETIT


## TOP 2: Stuko MIT


## TOP 3: Neue Ordnung

## TOP 4: Gremien ab Oktober 

## TOP 5: FÜS 


## TOP 6: Sonstiges


---

#### Sitzungsende




