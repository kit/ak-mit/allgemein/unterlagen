# AK-MIT Sitzungsprotokoll 25.01.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Feline, Max

#### Anwesende

[comment]: # (AK-MITler)

Robin, Carl, Magdalena, Max, Manuel, Feline, Antonio

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena

#### Sitzungsbeginn

13:05 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Die Protokolle werden noch überarbeitet auf Git.


## TOP 2: Berichte

#### FS-MACH/CIW

Nichts

#### FS-ETIT

Survival-Etit-Veranstaltung soll das nächste Mal mehr von uns mitorganisiert werden und die Etits sollen mehr mit uns zusammenarbeiten und z.B. unsere Vorschläge zu den Folien annehmen.
Nächstes Mal vielleicht auch mehr Studis reden lassen.

Einen Lernplan vorzustellen wäre vielleicht gut, nicht nur die negativen Aspekte.
Gut waren danach die Altklausuren und das get2gether.

Wichtig nochmal als Tipp: Die Wiederholungsfrist für Nachprüfungen sind zwei Semester.

## TOP 3: Neue SPO MIT

Vorschläge für den neuen Studienplan:

- TM 1-4  wird zu TM 1-3
- MKL 1-4 wird zu MKL A-C und nur A dann verpflichtend (statt MKL 1 und 2)
- Einführung von MKL s, eine Version extra für Mechatroniker*innen (als Grundlage)
- Felder und Wellen für MIT nur bis Weihnachten, der danach gelesene Wellen-Teil entfällt für MIT.
- MSUP hat ein ECTS mehr (aber wo landen die ECTS? in der Überfachlichen Qualifikation?)

Diesen Vorschläge mit ihrer genauen Umsetzung sollen noch besprochen werden.

Diese Umstellung könnten dafür sorgen, dass es für MITler*innen noch schwieriger als bisher wird in den Master Maschinenbau zu wechseln (zu viele Auflagen). 

StuKo Vortreffen 26.01.2023 13:00 Uhr im Sitzungsraum.

## TOP 4: Jahrgangstreffen MSUP

Oli wurde schon eine Email geschrieben; warten auf eine Antwort.
Was soll passieren?
Absprache Oli z.B. wer soll es zahlen? und kann man die MSUP-Räume nutzen?

## TOP 5: FÜS

FÜS soll regelmäßiger sein, um Infos auszutauschen

nächste FÜS 08.02.2023

Themen:

- Helmholtzprogrammkommission
- O-Phase
- Master SPO

## TOP 6: Sprechstunden

- 27.01.2023: Magdalena
- 03.03.2023: Carl
- 10.03.2023: Magdalena
- 17.02.2023: Feline
- 24.02.2023: Feline

## TOP 7: Sonstiges


**Nächste Sitzung am Mittwoch, 01.02.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

14:00 Uhr
