# AK-MIT Sitzungsprotokoll 28.10.2022

#### Sitzungsleitung

Feline Pirchmoser, Max Zuidberg

#### Anwesende

Max Zuidberg, Feline Pirchmoser, Carl Bodmann, Thorben Bruhns, Magdalena Janocha, Robin Helbig, Johann Fox, Manuel Wild, Simon Riedel, Jan Rösler(ETIT), Michel Brodatzki (ETIT), Wendelin Karg (ETIT)

#### Protokollierende Person

Carl Bodmann

#### Sitzungsbeginn

17:05 Uhr 

## TOP 0: Protokoll

Max Zuidberg und Feline Pirchmoser begrüßt alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

## TOP 1: Genehmigung der Tagesordnung

entfällt

## TOP 2: Genehmigung der Protokolle

Letzte Sitzung wurde kein Protokoll erstellt. In Zukunft werden Protokolle nach Annahme auf GitLab im [Unterlagen/Sitzungen](https://git.scc.kit.edu/ak-mit/unterlagen/-/tree/main/Sitzungen) Ordner abgespeichert.

## TOP 3: Berichte

#### FS-MACH/CIW: 

keine Berichte

#### FS-ETIT

Hiller hat neue SPO angestoßen (geplant für WS23/24). Bisher gibt es nur einen ersten Entwurf und wenig Details. Man sucht jetzt das Gespräch mit Hiller und wird den AK-MIT auf dem Laufenden halten. 

Die stud. Mitglieder des AK-MIT nehmen das Thema mit auf die nächste MIT Stuko. 

#### O-Phase

Feedback von Robin (O-Phasen Teilnehmer): Die ETIT O-Phase teilt die Studierende gleich zu Beginn der O-Phase in Gruppen ein. Da die Mechatroniker zu der Zeit bei der MACH O-Phase sind, verpassen sie die Einteilung und finden daher schwer Zugang zu den ETIT Gruppen. Eine bessere Abstimmung zwischen den Fachschaften ist gewünscht. 

## TOP 4: Sprechstunde

Ab dem 28.10 bis zum 18.11 werden von 12:30 Uhr bis 13:30 Uhr in der FS ETIT jeden Freitag (Ersti-)Sprechtunden angebotem. Ziel ist es danach weiter Sprechstunden anzubieten. Weitere Termine werden monatlich auf der AK-MIT Sitzung beschlossen. 

Besetzung der nächsten Sprechstunden: 

* 04.11 : Magdalena, Feline, Carl
* 11.11 : Magdalena, Feline, Carl
* 18.11 : Magdalena, Feline, Carl

## TOP 5: Stuko Vernetzung

* Die neuen MACH und ETIT SPOs werden auch Änderungen am MIT Studiengang erforderlich machen. Sollten wir nicht die Initiative ergreifen, besteht das Risiko, dass diese Änderungen im MIT Studiengang nur durch Modulhandbuchänderungen abgebildet werden (statt einer neuen SPO). Wegen der sehr umfangreichen Änderungen riskiert der MIT Studiengang (nahezu) unstudierbar zu werden. Für eine effektive Abstimmung der SPOs ist eine engere Zusammenarbeit zwischen den stud. Mitgliedern der Stukos MIT, ETIT und MACH nötig. 
* Derzeit findet kaum Austausch zwischen den einzelnen Stukos statt. Dies war vor ein paar Jahren noch anders  und man möchte diesen Austausch wiederherstellen. Dazu zählt: Gegenseitige Einladung auf Stuko Vortreffen und Austausch der Protokolle. ETIT Sitzungseinladung an info@, ETIT Sitzungsprotokolle an info@, MACH genauso. 
* Siehe auch: *Beschluss über den Arbeitskreis Mechatronik und Informationstechnik* (ausgearbeitet vom AK Quadrat MIT), der von beiden Fachschaften am 17.01.2018 angenommen wurde. 

## TOP 6: Webseite, SharePoint, Mailman

* Die [AK-MIT Webseite](https://ak-mit.vs.kit.edu) ist bei den Erstis nach wie vor wenig bekannt. => Mail an Erstis mit Werbung für Webseite
* Der AK MIT SharePoint wird von Max Zuidberg auf GitLab neu angelegt. Alte Dokumente werden mit umgezogen und neu sortiert (Thorben hilft). (Siehe [Sonstiges](#sonstiges) für Termine der Arbeitstreffen)
    * Protokolle und öffentliche Dokumente sind öffentlich zugänglich. Auf Unterordner der Gremien (Stuko, PA, Leitungen) haben jeweils nur die in der FÜS Gewählten Studentischen Vertreter. Diese werden nach jeder Wahl neu zugeteilt und selbst verwaltet.
    * Alles was öffentlich sein kann ist auch öffentlich. Ein Ort für interne Unterlagen fehlt noch.
    * 
* Verteiler werden derzeit beim ASTA gehostet und mit Mailman 2 verwaltet, was ohne Zugang zu den Servern nur eingeschränkte Möglichkeiten bietet. Daher Anfrage an die ASTA Admins für ein Upgrade auf Mailman 3. Rückmeldung erfolgt voraussichtlich in 1-2 Wochen. 

## TOP 7: Emails

* Auf FüS in Gremien gewählte Personen müssen Fachschaftsmailadressen erhalten. Die Verwendung privater Mailadressen ist i.d.R. aus Datenschutzgründen nicht erlaubt. 
    * Dies hat in den letzten Semestern nicht immer geklappt. Insbesondere die FS MACH hat allerdings Maßnahmen ergriffen, um unkompliziert entsprechende Mailadressen anzulegen. 
* Alle AK-MIT Mitglieder sollten den Anzeigename im Mailpostfach anpassen, damit sie als AK-MITler*innen erkennbar sind, z.B. *Name Nachname (AK-MIT)*.
* Zusätzlich sollten alle Mitglieder eine einheitliche Signatur konfigurieren. Thorben sendet eine entsprechende Vorlage an info@. Sie wird später in den internen Unterlagen auf GitLab hochgeladen.
* E-Mail Weiterleitung politische/aktivistische Themen
    * Grundsätzlich verteilt der AK-MIT keinerlei politische Themen oder Werbung aller Art (Events, Stellenausschreibungen, ...) an die Jahrgangsverteiler. Wird der AK-MIT von entsprechenden Organisationen kontaktiert, verweist er sie an die beiden Fachschaften. 
    * Entsprechende Mails an info@ wurden bisher von der AK-MIT Leitung bearbeitet und nicht an den info@ Verteiler durchgelassen. Es wurde beschlossen fortan alle Mails außer offensichtlichen Spam an info@ durchzulassen, damit alle AK-MTI Mitglieder darauf antworten können. Eine Antwort mit Standardtext ist möglich. Bei Antworten ist stets der info@ Verteiler ins CC zu nehmen, damit die anderen Mitglieder wissen, dass geantwortet wurde. 

## TOP 8: Sitzungsraum

* Derzeit hat der AK-MIT keinen eigenen Sitzungsraum. Da die AK-MIT Sitzung wieder regelmäßig stattfindet, wird dringend wieder ein fixer Raum benötigt. 
* Die anwesenden FS-ETIT Fachschaftler berichten, dass der ehemalige Sitzungsraum (Seminarraum 016 am ETI) wieder verfügbar ist.
    * -> Anfrage an Prof. Hiller, ob er wieder vom AK-MIT für seine Sitzungen genutzt werden kann. Ansonsten Anfrage an Prof. Matthiesen für einen Raum am IPEK

## TOP 9: Sonstiges

* Arbeitstreffen: Montags 13:00 - 15:30 + Sprechstunde freitags 
* Sprechstunde auf Webseite und in Kalender hinterlegen + gut kommunizieren
* Carl merkt an, dass das Protokollieren in LaTeX zu aufwändig ist. Es wird beschlossen fortan in Markdown zu protokollieren. 
* Nächster Sitzungstermin: 
    * Sitzung fortan mittwochs in der Mittagspause (13:00-14:00)
    * Sitzungsort kann erst nach Klärung der Raumsituation kommuniziert werden.
    * **Nächste Sitzung am 09.11.2022 13:00**

---

#### Sitzungsende

18:12 Uhr
