# AK-MIT Sitzungsprotokoll 10.05.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

 Max, Magdalena, Thorben, Carla, Lorenz



[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena Janocha

#### Sitzungsbeginn

17:05 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg  begrüßt alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Von der letzten Sitzung (03.05.2023) soll das Folienprotokoll noch hochgeladen werden. Thorben, Johann will sein Protokoll überarbeiten. Die offenen Protokolle wurden alle angenommen.



## TOP 2: Berichte

#### FS-MACH/CIW

- Nächste Woche ist Fatama.
- Nächste Woche ist Dienstag Sitzung.

#### FS-ETIT



## TOP 3: SPO MIT

- gab einen Bericht in der Sitzung von der Fachschaft Mach
- gute und schlechte Punkte der neuen SPO
  - im 1. Semester soll die Workload verringert werden (Reduktion von TM, Systemmodellelierung im 1./2. Semester; TM 1/2 evtl. zusammenlegen - Probleme mit der O-Prüfung)
  - die Klausurenphase ist im 1. Semester sehr hart
  - Wenn TM2 weniger ECTS, dann eventuell mehr ECTS für MKL
  - Anmerkung: Bei einer Annahme dringend daraufhinweisen, dass die Zeit zwischen den Prüfungen sehr knapp und deswegen müssen sich die Prüfung mit ihrem Stoff ändern(Prüfungsanforderungen)
  - ansprechen das die Prüfungen in einem Wochenrhytmus geschreiben werden soll, bei Mach
  - SPO ist noch unsicher (Wahlbereich noch unklar)
  - Stellung über, wie wir den Wahlbereich gestaltet haben wollen, überlegen (Wahlblöcke oder jedes Fach einzeln wählbar); Inspiration aus dem Master, Bachelor Mach


-dringend vor der Stuko mit denen schon besprechen
  - vor der Stuko muss es einen Vorschlag geben, mit dem wir zufrieden sind
  - Antwort auf die E-Mail


- Reminder: Treffen zwischen Fakrat und Stuko machen(Dienstag); die neusten Information mit Fakratmitglieder besprechen: Uhrzeit abklären: 10Uhr?

- Spielraum durch MHB Änderungen
  - Eingeschränkt dadurch, dass der Pflichtbereich im Umfang nicht ändern kann. 
  - Nutzen um die SPO in den nächsten Semestern zu verbessern. 

- Die AK-MIT Sitzung sieht es als nicht sinnvoll an, die SPO abzulehnen. Man ist mit vielen Punkten nicht glücklich, aber keiner rechtfertigt eine Ablehnung. 
  - Späte Planung
  - Schlechte Kommunikation
  - Anspruchsvolles Semester mit Kompromissen

- Problematisch: Nach wie vor ETIT Wahlfächer unbekannt. 
  - Prof. Hiller fragen


## TOP 4: O-Phase

- Verantwortliche\*r gesucht
    - MIT Grillen
    - MIT Kneipentour
    - Infoveranstaltung
    - Absprache FS MACH / FS ETIT

- Diskussion über allgemeine Probleme bei den O-Phase in den vergangenen Jahren.
    - Explizite Hinweise in den Veranstaltungen falls MITler*innen nicht anwesend sein sollten. Bei Veranstaltungen beider Fachschaften.
    - Beiden Fachschaften den MIT Verantwortlichen vorstellen und auf Austauch pochen. 

- Nächste Sitzung: Learnings aus den vergangenen O-Phasen vorstellen. 
Nachtrag: Protokoll gibt es auf dem Fachschaftsserver Mach

## TOP 5: 2. Semester Infoveranstaltung

- Keine\*r der Anwesenden hat Zeit. Thema vertagt. 

## TOP 6: Sonstiges

- Sind wöchentliche Sprechstunden sinnvoll?
    - Kontra:
        - MIT spezifische Fragen können an info@ verwiesen werden, ansonsten sind die Fachschaftssprechstunden für das meiste ausreichend. 
    - Pro:
        - Austausch mit den Fachschaften
        - Eine Anlaufstelle nur für MITler\*innen
    - Vorerst bleiben wir beim aktuellen System, behalten es aber weiter im Blick

- Raumbuchung Sitzung
    - Die Sitzung möchte vorerst beim neuen Termin bleiben. 
    - Max Zuidberg kümmert sich um die Raumbuchung. 

**Nächste Sitzung am Mittwoch, 17.05.2023 17:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

18:30 Uhr
