# AK-MIT Sitzungsprotokoll 08.03.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Feline Pirchmoser, Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

Robin, Carl, Max, Magdalena, Feline, Thorben



[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)

#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)


Magdalena

#### Sitzungsbeginn

13:09 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)



## TOP 2: Berichte

#### FS-MACH/CIW
Heute Abend wird die finaler Ablauf der Master-Ophase festgemacht.
Die finale SPO soll am Mittwoch über die StuKo Mach.


#### FS-ETIT


#### LAZ-Bericht
-Montag treffen wegen LAZ, um mit beiden Fakultäten mal geredet zu haben

-Räume im LAZ, zur Einteilung (kam eine E-Mail)

-gibt zwei Räume

-Grundsätzlich das Gespräch mit beiden Fachschaften suchen, um heruas zu finden, wie man die Raumsituation am besten löst.

-Für den AK MIT grundsätzlich ein Raum notwendig für die Sitzung, sonst eigentlich keiner

-soll als Top auf die Mschienenbausitzung

-der große Raum vielleicht als Lernraum?


## TOP 3: SPO MIT / StuKo 01.03.2023
-technische Ethik kommt sehr wahrscheinlich

-Update kommt, wenn Ergebnisse

-bestimmen eines Verbindungsprofs, um die Kommunikation zu erleichtern

-neue Fächer vielleicht Automatisierungstechnik

-Maschienbau-SPO ist fest

## TOP 4: FDIBA Doppelabschluss
-Programm bei dem es zu einem Austausch zwischen den Unis kommt und die Studierenden ein Jahr an der anderen Uni verbringen

-Parnterschaft mit der Universität Sofia

-Enge Kooperation

-Vertrag soll verlängert werden

-generell Werbung für das Programm, da wenig Teilnehmer vorallem vom KIT

-Vorschlag: Änderung dieses Programms, aufgrund von MSUP. Die Planung dort ist hinfällig. Die Zeit
aufteilung trifft vorallem die Mechatroniker/innen in MSUP hart. Nachteil für diese Gruppe.

-zeitlich für die Mechatronikstudis schwierig

-Umsetzung soll überarbeitet werden

## TOP 5: 

Erasmus Bewerbung

-Bisher war relevant welcher Fakultät man angehört. Jetzt zählt nur noch bei welcher 
Fakultät man den Antrag stellt.


Infoveranstaltung 4. Semester

-Themen wie Praktikum und Bachelorarbeit, Übergang Master/Bachelor
-Ende Mai/ Anfang Juni


Wahlveranstaltung für die 2. Semester

-Erasmusinfos

-->Treffen mit Jasmin, um alles abzuklären


AK-MIT Teambuilding

Kochen oder Essen gehen
Doodle-Umfrage ausfüllen


Campustag 13.05.2023

-evtl Unterstützung der Fachschaften


Sprechstunden

besprechen, wenn wieder Uni anfängt



**Nächste Sitzung am Mittwoch, 29.03.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:59 Uhr
