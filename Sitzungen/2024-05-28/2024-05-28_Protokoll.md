# AK-MIT Sitzungsprotokoll 28.05.2024

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Carla und Carl

#### Anwesende

[comment]: # (AK-MITler)

Max B., Carla, Carl, Matthias, Tillman, Robin, Thorben, Matthias, Marco
 

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)
Carla 

#### Sitzungsbeginn
13:09

---

## TOP 0: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

## TOP 1: Berichte

Änderung der AK MIT Satzung in Arbeit. Soll auf beiden FS-Sitzungen vorgestellt und Feedback eingeholt werden. Anschließend ist ein Beschluss auf FÜS vorgesehen. 

#### FS-MACH/CIW
Sommerfest ist in Planung, wer Interesse hat, darf sich melden. Auch beim Unifest Fachschaftsstand darf gerne geholfen werden. Eine Campushütte ist in Planung. 

#### FS-ETIT
Campus-FaWo hat stattgefunden - leckeres Essen, Schwedenstühle wurden gebaut, introspektiv Workshops. Die Ergebnisse werden vom Vorstand noch aufgearbeitet und dann auf ETIT Sitzung vorgetsellt. 
BuFatA steht an.   

## TOP 2: O-Phase
- Valentin, Fabian und Matthias haben Interesse
- 1. Woche, Di Mittag: Infoveranstaltung
- 1. Woche, Di/Mi: MIT Grillen 
- 2. Woche: MIT Kneipentour

ETIT
- 1. Woche, Mo: Grillen im LTI -> die Sachen am LTI 
- Wann die O-Party stattfindet, entscheidet sich am Donnerstag
- wünscht sich ein Leitfaden für MIT Studis

Allg
- Wünschenswert wäre eine gemeinsame E-Mail an MIT-Studis, damit sie nicht verwirrt sind, an welchen O-Phasen sie teilnehmen soll. 
- ical Kalender beider Fachschaften wünschenswert 

## TOP 3: Bierinsel
Findet am 04.06.2024 ab 14 Uhr statt. 

## TOP 4: Master Zugangskommission 
- HiWi für Raussuchen der Fächer fängt jetzt an 
- Vierzehntägiger Termin ab 10.06. bis Ende September 
- Zusätzlich zu Carl und Thorben, interessiert sich Max B. dafür

## TOP 5: Bedeutung Fakultätszugehörigkeit 
- Für den normalen Studi: kein Unterschied ob MACH oder ETIT
- Für das Engagement in den Fachschaften: relevant für FakRat, Vorstand, ETIT Sitzung; nicht relevant für Stuko, PA, Zulako



## TOP 6: Sonstiges
Nächste Termine: 
- 04.06. Bierinsel 
- 14./15.06. Unifest
- 27.06. FMC Sommerfest
- tbd FMC Campushütte
- 10.07. FüS

---

#### Sitzungsende
13:57



