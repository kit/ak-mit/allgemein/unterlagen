# AK-MIT Sitzungsprotokoll 24.05.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max

#### Anwesende

[comment]: # (AK-MITler)

Max, Magdalena, Thorben, Carla, Robin

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena

#### Sitzungsbeginn

17:08 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Thorben liest sich zu seinen Protokollen seine Änderungen durch.

## TOP 2: Berichte

#### FS-MACH/CIW

- Die Fatama hat stattgefunden.
- Folien die vom dem Nachhaltigkeitsrat ans Präsidium gegangen sind, wurden angeschaut 

#### FS-ETIT

- 10-Tage Öffungszeiten der Etit-Fachschaft; wir wollen da auch einen Tag übernehmen (Hinweis: Unifest kollidiert evtl. zeitlich)
- Fachschaften sollen sich selbst auch Folien über Nachhaltigkeit überlegen

## TOP 3: SPO MIT

- siehe Folien

-Beschlossen: die Stellungnahme soll erst nach dem Fakultätsrat Mach verschickt werden

## TOP 4: Bachelorauswahlkommission

- Infos siehe Folien (Termine, Kandidaten)
- To-do: Email schreiben um genauere Informationen zu bekommen bzgl. genaueren Zeiten und ob es online ist)
- Robin ist zu dem Zeitpunkt im Urlaub, Magdalena kann nicht zu den Terminen, Thorben schreibt danach Klausur


## TOP 5: FüS

- 21. Juni Terminvorschlag?
- danach ist Unifest (Fach MACH/CIW wird nicht stark vertreten sein?)
- AK-MIT muss sich um FüS kümmern

- 21. Juni als Termin angenommen

To-Do: Bis zum 9. Juni muss die Einladung spätestens raus


## TOP 6: Sprechstunden

[comment]: # (Dieser TOP kommt nicht auf jeder Sitzung vor; bei Bedarf einfach löschen.)

[comment]: # (Nachfolgenden Satz anpassen falls nötig! Die FS wechselt i.d.R. jedes Semester.)

Die Sprechstunden finden jede Woche mittwochs um 13:00-14:00 Uhr in der FS ETIT statt. 

Besetzung der nächsten Sprechstunden, klären wir nach der Sitzung.


## TOP 5: Sonstiges

- Es gibt Workshop AK-MIT auf der Fachschaftshütte Mach.
- Sitzungsvorlage Zeit ändern


**Nächste Sitzung am Mittwoch, 31.05.2023 17:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

17:40 Uhr
