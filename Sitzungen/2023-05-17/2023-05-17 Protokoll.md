# AK-MIT Sitzungsprotokoll 17.05.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max

#### Anwesende

[comment]: # (AK-MITler) 

Max, Johann, Magdalena



[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena Janocha

#### Sitzungsbeginn

17:12 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Die Annahme des Protokolle wurde vertagt, weil wir nicht beschlussfähig sind.

## TOP 2: Berichte

#### FS-MACH/CIW
Grade nichts


#### FS-ETIT
Fachschaftswochenende hat stattgefunden.


## TOP 3: Ämter

Bachelor Auswahlkomission
- wir suchen 6 Leute, brauchen eigentlich nur 4, damit Ersatz bleibt
- ist ein Nachmittag, aber der Termin steht noch nicht fest
- mögliche Kandidaten: Robin, Johann Fox, Feline, Magdalena, Jan Rössler

Frage nach der Weiterführung der Ämter:
- Nachfolger für Max (Leitung & Stuko)
- bis Semesterende sich darüber Gedanken machen, welche Ämter man übernehmen will/abgibt
- Robin zieht es in Erwägung in die Stuko einzuziehen

## TOP 3: Treffen mit Prof. Geimer

- Inhalt liegt auf dem Mit-Git unter Notitzen
- in der Zukunft evtl. Umbau von TM1/2, um Strömungslehre mit zu integrieren
- soll mehr aus Maschienbau mitgenommen werden als nur TM

## TOP 3: Treffen mit Prof. Boehlke

- es ging vorallem um das 1. Semester in der neuen SPO
- 1 ECTS könnte man streichen, aber ist schwierig in der Umsetzung (Themen streichen (Einschränkung in der Klausur), Themen nicht in der Tiefe bearbeiten (sehr verwirrend in der Vorlesung))
- will mit den anderen Professoren reden, da er dieses Problem auch sieht
- mal schauen was passiert bis zur Deadline am Montag
- Klausuren müssen sich nicht ändern, trotz verkürztem Prüfungszeitraums


## TOP 4: neue SPO

Haltung zur neuen SPO:
- Johann hält dies SPO für studierbar, aber natürlich Optimierungspotential und je nach Entscheidung soll der Weg optimal ausgenutzt werden (Timing)

Meinungsbild:
- Wenn TM einen Punkt bekommen, dann soll der an MKL gehen
- Wenn wir 2 Punkte bekommen, dann ziehen wir Systemmodellierung vor

## TOP 5: Sonstiges



**Nächste Sitzung am Mittwoch, 24.05.2023 17:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

18:05 Uhr
