# AK-MIT Sitzungsprotokoll 21.06.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

Feline Pirchmoser (online), Magdalena Janocha, Robin Helbig (ab 17:27 Uhr), Johann Fox, Max Zuidberg

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Johann Fox

#### Sitzungsbeginn

17:21 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Die Annahme der letzten Protokolle der letzten Sitzungen wurde aufgrund mangelnder Korrekturen vertragt. Grund: Stress.

## TOP 2: Berichte

#### FS-MACH/CIW

Feline war bei der letzten Sitzung der Fachschaft MACH/CIW anwesend und es gibt keine Neuigkeiten, die für den AK MIT relevant sind.

#### FS-ETIT

Max war bei der letzten Sitzung der Fachschaft ETIT anwesend. In der letzten SKOM (ETIT) kam das Thema Vorlesungsevaluation auf. Alle Infos dazu sind den Unterlagen des vergangenen Fachschafts-Wochenendes der ETIT-Fachschaft zu entnehmen.

## TOP 3: AK MIT Tag bei den 10-Tage Öffnungszeiten der FS ETIT

Der aktuelle Stand der Planung für den 29.06.2023 ist dem GIT zu entnehmen.

17:27 Uhr: Robin Helbig erscheint.

Die reguläre Sprechstunde wird von 18:00 bis 20:00 Uhr stattfinden.

- Ab 20:00 Uhr wird die Fachschaft MACH/CIW eingeladen.
- Die Lasershow wird ab 0:00 Uhr stattfinden.
- Max hat mit dem ITIV bezüglich der Segways gesprochen: Für die Segway-Tour stellt das ITIV zwei Segways zur Verfügung.
- Prof. Matthiesen und Prof. Doppelbauer wurden beide eingeladen, allerdings sind beide verhindert.
- Die Finanzierung für Cocktails, Frühstück und Essen ist geklärt im angedachten Rahmen.

## TOP 4: Sonstiges

Magdalena regt an, "mehr Spaß" außerhalb der Sitzungen zu haben. Dazu würden sich Events anbieten, die nicht Teil der Sitzung sind. Die kommt gut an, es muss sich nur jemand finden, der Events organisiert.

**Nächste Sitzung am Mittwoch, 28.06.2023 17:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

17:49 Uhr
