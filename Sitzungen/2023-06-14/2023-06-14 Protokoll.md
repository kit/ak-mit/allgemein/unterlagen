# AK-MIT Sitzungsprotokoll 14.06.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Feline Pirchmoser, Max Zuidberg

#### Anwesende

[comment]: # (AK-MITler)

Feline, Thorben, Carla, Max

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)



[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Max Zuidberg

#### Sitzungsbeginn

17:00 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. 

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Annahme von Protokollen wird aufgrund mangelnder Korrekturen vertagt.

## TOP 2: Berichte

#### FS-MACH/CIW

Carla berichtet vom AK-MIT-Kommunikations-Workshop auf der Fachschaftshütte. 

#### FS-ETIT

Prof. Mike Barth kommt am 22.06.2023 11:00 in die Fachschaft um diese kennenzulernen und sich mit den Studierenden auszutauschen. Es sollen auch 1-2 AK-MITler*innen anwesend sein. 

## TOP 3: AK-MIT-Tag auf der 10-Tage-Öffnungszeit der FS ETIT

Max berichtet vom Planungstreffen mit Magdalena am Montag. Der derzeitige Plan ist auf GitLab unter Intern zu sehen. 

Carla fragt, wie es mit Essen abends aussieht. => Muss noch geklärt werden; abklären, wie die Fachschaft die Verpflegung allgemein handhabt. 

## TOP 4: O-Phase

* Events
	* Infoveranstaltung
	* MIT-Grillen
	* MIT-Kneipentour
* Orga Zeugs
	* Bändchen organisieren
* Infos
	* O-Phase-Webseite auf AK-MIT Seite
	* Nimm's Mit
	* Master Leidfaden
	* Bachelor Leidfaden
	* Nimm's Mit Email für Leute bei der Zulassung
* Orga
	* Carla (MACH Kontakte)
	* Thorben: Kümmert sich um die Grillgenehmigungen
	* Robin (ETIT)
	* Leidfäden
		* Robin (Sprechstunden)
		


## TOP 5: Sonstiges

Carla fragt, ob bereits auf den Studiengangswebseiten die SPO Änderungen erwähnt werden, da Bewerber*innen sonst einen ganz anderen Studiengang im Oktober erwartet. 

	* Vermutlich nicht. Klären, wer dafür zuständig ist, und ansprechen. Z.B. Jasmin Azak
	* Mit den Fachschaften abklären, damit es zumindest auf deren Webseiten erwähnt wird. 
	


**Nächste Sitzung am Mittwoch, XX.XX.XXXX 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

XX:XX Uhr
