# AK-MIT Sitzungsprotokoll 19.04.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Von Frauen geleitete Sitzung: Feline Pirchmoser
Max Zuidberg (von Frauen unterdrückt)

#### Anwesende

[comment]: # (AK-MITler)

Magdalena Janocha
Feline Pirchmoser
Robin Helbig
Johann Fox
Max Zuidberg (Identität ungeklärt)

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

niemand

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)

niemand

#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Johann Fox (von Frauen approved)

#### Sitzungsbeginn

13:12 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Protkollannahme wurde auf Antrag von der weiblicher Sitzungsleitung vertagt.

## TOP 2: Berichte

#### FS-MACH/CIW

#### FS-ETIT

Max berichtet von einer der letzten FS ETIT-Sitzungen. Bei dieser Sitzung wurde viel über den neuen O-Phasen-Kodex diskutiert. Hier wurde ein Verbot von Kleiderketten bei O-Veranstaltungen diskutiert und ein Verbot von "Alkohol-Zwang".
Hintergrund des O-Phasen-Kodex des AStA ist dem entsprechenden E-Mail-Verkehr zu entnehmen. Bisher steht das nur zur Diskussion.
Der AK-MIT kann hierzu nichts beitragen und deligiert die Verantwortung dieser Diskussion an die Fachschaften.

Technische Zwischenfrage von Robin: Wer organisiert denn die O-Phase und wer kümmert sich um MIT-eigene Veranstaltungen?
- Es gibt MIT-Grillen (FS MACH/CIW würde für den AK-MIT mit einkaufen, wenn sie Bescheid wissen)
- Eine eigene Kneipen-Tour (Kneipen müssen nur organisiert werden)
- Eigene MIT-Einführungsveranstaltung (hier würde es sich anbieten, Prof. Matthiesen anzufragen)

Robin bietet sich an, O-Phasen-Verantwortliche*r zu werden. Die Sitzung nimmt Robins Angebot dankend an.

## TOP 3: Klausurtermine Wintersemester 2023/2024

Hierzu kam am 19.04.2023 eine Mail.

**Todo Feline: Bei FS MACH/CIW wird sie ansprechen, inwieweit hier Terminkollisionnen mit den Terminen der ETIT-Fakultät vermieden werden können.**

## TOP 4: Viert-Semester-Info-Veranstaltung

Max schaut sich die Folien an, Feline geht hin. Es geht um die von Frauen-geführte Veranstaltung von Jasmin Azak.

## TOP 5: Sprechstunden

[comment]: # (Dieser TOP kommt nicht auf jeder Sitzung vor; bei Bedarf einfach löschen.)

[comment]: # (Nachfolgenden Satz anpassen falls nötig! Die FS wechselt i.d.R. jedes Semester.)

Die Sprechstunden finden in diesem Semester in der Fachschaft MACH/CIW statt. Die Idee ist, dass zusätzlich zur normalen Sprechstunde auch MITler da sein sollen.
Clever wäre hierbei nur solche Termine anzubieten, zu denen MITlern auch keine Vorlesungen haben.

**Todo Feline: in der FS MACH/CIW-Sitzung abstimmen, welcher Tag sich für eine MIT-Sprechstunde am besten eignet.**

## TOP 5: Sonstiges

### StuKo MIT

Die nächste StuKo MIT findet am 21.04.2023 statt.

### AK-MIT-Team-Building

Am 20.04.2023 findet um 19:00 Uhr ein AK-MIT-Team-Building im Oxford Pub. Dazu sind alle herzlichst eingeladen.

### Sitzungstermin AK-MIT

Der Sitzungstermin des AK-MIT soll auf Antrag der weiblichen Führungsriege für das kommende Sommersemester verlegt werden. Feline möchte den Termin auf mittwochs, 18:00 Uhr verlegen, damit sie einen großen "Sitzungsblock" mittwochs hab. Max ist aus Prinzip gegen den Vorschlag, da die AK-MIT-Sitzungen sonst keinen zeitlichen Puffer hätten. Für Magdalena ist der Vorschlag von Max aus Prinzip abzulehnen, da sonst irgendwas bei ihr kollidieren würde.
Das Thema neuer Sitzungstermin wird auf Antrag des Protokollanten vertagt.

**Nächste Sitzung am Mittwoch, 26.04.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**
"Nächste Woche so wie diese Woche"

---

#### Sitzungsende

13:45 Uhr
