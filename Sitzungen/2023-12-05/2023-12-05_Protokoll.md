# AK-MIT Sitzungsprotokoll 05.12.2023

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Feline Pirchmoser

#### Anwesende

[comment]: # (AK-MITler)

Feline, Carla, Magdalena, Max, 

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

Theresa

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Max

#### Sitzungsbeginn

13:05 Uhr

---

## TOP -1: Einführung

Feline stellt den AK-MIT und seine Arbeitsweise vor. 

## TOP 0: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

## TOP 1: Berichte

#### FS-MACH/CIW

Theresa berichtet. 

- Donnerstag ist Winterfest. Alle sind herzlich auf eine Waffel und Glühwein eingeladen. Mischung aus Weihnachtsmarkt und Glühweinrave
- Es wird am Master MACH gearbeitet. Der AK-MIT ist zu den Vortreffen eingeladen, 

#### FS-ETIT

Magdalena berichtet. 

Problem: Institute geben Nachprüfungstermine jetzt verpflichtend raus. Können Studierende an diesem Termin nicht, so verlangen sie streng(er)e Nachweise. Bisher war es wohl so, dass die Studierenden einen Terminvorschlag machen konnten. Man trifft sich heute noch in der FS um das Thema heute zu besprechen. 

Theresa: Bei MACH kriegt man auch vom Institut aus eine Mail mit dem Nachprüfungstermin. 

Magdalena unterstreicht, dass es wichtig ist, dass es für MITler\*innen einheitleich bleibt. 

#### Weitere Berichte

- Carla erkundigt sich, wie es mit den Tutorien-Termine gelaufen ist. 
	
	Bei den beiden Erstis gab es keine größeren Probleme. Die generelle Kritik am System bleibt, insbesondere, dass es als MITler\*innen nicht möglich ist, die Durchschnittsbewertung von 3 Sternen zu erreichen, ohne Termine anzugeben, an denen andere Veranstaltungen liegen. 
	
	Carla regt an, dass man versucht, für MITler\*innen andere Kriterien zu erreichen (dass MITler\*innen weniger angeben müssen). 

- Es wird das neue Fach besprochen, wie es kommuniziert wurde, und weshalb das Fach so liegt wie es liegt. Der generelle Rat lautet, die Klausur beim geringsten Zweifel zu schieben und erst im 3. Semester teilzunehmen. 

- Generelle Diskussion über Klausuren, Altklausuren, Änderungen durch die SPO. 

## TOP 2: StuKo

### Wahlbereich Bachelor 2023

Carla berichtet. 

- Der Wahlbereich wurde ggü. des bisherigen MHBs nochmal überarbeitet. 
- Es gibt 3 Wahlbereiche: ETIT, MACH, ETIT+MACH+INFO+WIWI
	- ETIT: min. 9 LP, keine vorgeschriebene Kombinationen
	- MACH: min. 9 LP
		- Thermo + Ströle
		- MKL B + C
		- Werkstoffkunde (CIW)
		- Produktionstechnik
	- Wahlbereich 3: auffüllen bis 35 LP

### Sonstiges

- Master ZuLaKo kriegt eine\*n HiWi

### Master SPO

Studierenden-Vorschlag (siehe Folien) wurde soweit von der StuKo angenommen. Es gab Ideen für weitere Vertiefungsfächer, diese wurden jedoch verworfen; es bleibt bei den existierenden Vertiefungsfächern. 

Wie bei ETIT wird angestrebt, den Studiengang komplett auf Englisch anzubieten. Es wird aber weiterhin deutschsprachige Vorlesungen geben, es wird allerdings nicht mehr möglich sein, den Master komplett auf Deutsch zu studieren. 

## TOP 3: Ersti-Weihnachtsfeier

Montag, 11.12.2023 17:30 im Kaffee- und Studierendenzimmer (KuSZ) im IBT (Geb. 30.33).

Robin hat mit Konstantin (Finanzen FS MACH/CIW) wegen der Finanzierung gesprochen, das sollte soweit geklärt sein. 

Fleißig Werbung machen! 

Es werden noch Helfende ab 16:30 gesucht!

## TOP 4: Website

Robin hat vor, die Webseite aktiver (um)zu gestalten. Details folgen. 

## TOP 5: Sonstiges

### Feedback O-Phase

- Schwierig zu unterscheiden was MACH und was ETIT ist. 
- Teils widersprüchliche Informationen, wo die MITler\*inenn hin sollen. 
	- Klare Empfehlungen gewünscht
	- Alternativ: *Beide* Kalender per iCal herunterladbar und/oder gemeinsamer Kalender. 
- Tendentiell eher mehr als weniger MIT-spezifische Veranstaltungen gewünscht. 
- 

### Daten

- FS MACH/CIW Winterfest 07.12.2023
- MIT Ersti-Weihnachtsfeier 11.12.2023 17:30
- MIT Wahlinfoveranstaltung am 30.01.2024

**Nächste Sitzung am Dienstag, 19.12.2023 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:52 Uhr
