# AK-MIT Sitzungsprotokoll 07.12.2022

[comment]: # (Kommentare wie diese müssen nicht entfernt werden. Sie werden von Markdown ignoriert.)

[comment]: # (Vor und nach einem Kommentar, einer Überschrift, einer Aufzählung immer min. 1 Zeile leer lassen. Überflüssige Leerzeilen werden von Markdown automatisch ignoriert, fehlende Leerzeilen können jedoch zu Darstellungsproblemen führen.)

[comment]: # (Alle Teile mit XX müssen geändert/ausgefüllt werden. Am Ende sollten keine XX mehr im Protokoll vorkommen. STRG+F ist dein Freund.)

#### Sitzungsleitung

[comment]: # (I.d.R. eines oder beide Mitglieder der AK-MIT Leitung)

Max & Feline


#### Anwesende

[comment]: # (AK-MITler)

Thorben, Carl, Magdalena, Tim, Feline, Leo, Max, Vanessa, Robin, Simon

[comment]: # (FS-MACH/CIW - Falls anwesend, "FS-MACH/CIW" mit angeben.)

[comment]: # (FS-ETIT - Falls anwesend, "FS-ETIT" mit angeben.)



#### Protokollierende Person

[comment]: # (Danke, dass du das Protokoll schreibst!)

Magdalena

#### Sitzungsbeginn

13:06 Uhr

---

## TOP 0: TO-Annahme

[comment]: # (Bei Bedarf anpassen, bzw. zusätzliche TOPs auflisten.)

Max Zuidberg und Feline Pirchmoser begrüßen alle Anwesenden. Es wird festgestellt, dass zur Sitzung ordnungsgemäß eingeladen wurde und die Sitzung beschlussfähig ist.

Tagesordnung per Akklamation genehmigt. 

## TOP 1: Protokoll

[comment]: # (Vorlage zum Kopieren: Das Protokoll der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde angenommen.)

[comment]: # (Vorlage zum Kopieren: Die Annahme des Protokolls der letzten Sitzung vom XX.XX.XXXX, geschrieben von XX wurde vertagt. Grund XX.)

Die Annahme des Protokolls der letzten Sitzungen, geschrieben von Magdalena wurde vertagt. Grund: Formatierung.



## TOP 2: Berichte

#### FS-MACH/CIW

Eintragen in die Winterfestschichten


#### FS-ETIT

* Feedback zur O-Phase wird rumgeschickt, evtl. auch intressant für uns.

* Die Ausführung/Erklärung der HAA(Rechtsabteilung des KITs) zur SPO-Änderung steht nicht so in der SPO. Nun ist aber ein Zusammenhang mit der zweiten Prüfung erlaubt(inhaltlich Zusammenhang); Ausführung jedoch nicht klar.

* Bezüglich der Änderungen zu Täuschungsversuchen. Der Prüfungssausschuss darf darüber entscheiden, ob es sich um eine Täuschung handelt. Wenn dieser beschließt das es sich um eine Täuschung handelt, gibt es kein Recht auf eine Nachprüfung.

* Man kann bis Jahresende Gelder beantragen für ehrenamtliche Projekte (z.B. Feste) im nächsten Jahr beim [Förderverein der verfassten Studierendenschaft (VS)](https://studierendenschaft.org/start). 



## TOP 3: Ersti-Weihnachtsfeier

* Geld soll bei der FS ETIT beantragt werden.
* Raumreservierung (Anfrage an den FS ETIT Vorstand)
* Raumvorschläge: Fachschaftsraum im IBT 4. Stock
* Zeit: 19.12.2022 17:30 Uhr
* Kosten ungefähr: ~50€, zur Sicherheit 100€ anfragen (besser zu viel als zu wenig)
* Glühweinkocher organisieren/reservieren
* Werbung nochmal für AK MIT, Fachschaft etc.

## TOP 5: Sonstiges

* Katharina Williams hat darum gebeten, dass wir Ihr mitteilen, wer von uns Abschlüsse (z.B. BSc) hat, damit Sie das auf der ETIT Fakultätswebseite eintragen kann. Ein AK-MITler hat bereits geantwortet, sich jedoch in den Titeln geirrt. <br>
  => Max schickt eine korrigierte Mail raus. <br>
  _Nachtrag: Titel auf der Webseite wurden am 07.12.2022 korrigiert._
* Bisher gab es getrennte Mailadressen für Bachelor- und Masterprüfungsausschuss (`bpa@...` und `mpa@...`). Bearbeitet werden allerdings alle Anträge in einem einzigen Ausschuss. Die doppelten Mailadressen führen zu vielen doppelten Emails. <br>
  Vorschlag: Umstellen auf eine einzige `pa@ak-mit.vs.kit.edu` Adresse. Alte Adressen sollen nicht mehr kommuniziert werden, dort eintreffende Mails werden auf den neuen Verteiler umgeleitet.  
* Wir suchen einen Webseitenbeauftragten. Die AK-MIT Webseite ist pflegeleicht, dennoch braucht es eine Person, die sich darum kümmert.


**Nächste Sitzung am Mittwoch, 14.12.2022 13:00 im ETI Seminarraum 016 (Geb. 11.10).**

---

#### Sitzungsende

13:50 Uhr
