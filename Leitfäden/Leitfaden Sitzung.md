# Leitfaden Sitzung vorbereiten

Wie du eine Sitzung samt Folien und Protokoll vorbereitest. Hauptsächlich relevant für die Leitung. 

Alle Schritte werden im AK-MIT/Allgemein/Unterlagen Repository durchgeführt.

- Wechsle zum Branch `vorlage-sitzung`
- Erstelle einen neuen Branch `sitzung-XXXX-XX-XX` basierend auf `vorlage-sitzung`. Mit XXXX-XX-XX ist das Datum der Sitzung gemeint. 
- Öffne den Ordner `Sitzung`. Du siehst einen Unterordner `XXXX-XX-XX`. Er enthält eine Protokoll- und eine Folienvorlage. 
	- Benenne den Ordner und die Dateien um. 
- Fülle die Folien mit den anstehenden TOPs aus. Du solltest sie ebenfalls im Protokoll ausfüllen.
- Füge ggf. weitere Dateien zu diesem Ordner hinzu.
- Erstelle einen Commit mit deinen Änderungen und push sie. 

Während/nach der Sitzung:

- Der/die Protokollant*in wechselt zum Sitzungsbranch und kann deine Protokollvorlage jetzt öffnen und ausfüllen. 
- Das vorläufige Protokoll kann und sollte in den gleichen Branch gepushed werden. 
- Die protokollierende Person erstellt einen Merge Request für den Sitzungsbranch. Alle Mitglieder werden darüber benachrichtigt und können Änderungen einreichen. 
- Sobald das Protokoll auf einer Sitzung beschlossen wurde, führt ein Leitungsmitglied den Merge Request durch. 
