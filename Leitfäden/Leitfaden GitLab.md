# Workflow GitLab

## Motivation

Ganz im Sinne von GitLab ist es uns wichtig, dass Dateien nicht einfach so verändert werden, sondern Veränderungen stets dokumentiert und reviewed werden. Diese Funktion ist für "normale" Cloud Dienste eher ungewöhnlich, weshalb auch häufig noch Protokolle im Umlaufverfahren bearbeitet und korrigiert werden. 

Das versuchen wir hiermit zugunsten eines besseren Systems überflüssig zu machen. 

Tutorials zu Git (GitHub, GitLab, GitWasAuchImmer) Workflows gibt es viele; sicherlich auch viele bessere als dieser. Daher gerne mal herumstöbern im Netz. 

## Grundlagen

### Was ist Git

*Dieses Kapitel basiert auf meiner Arbeit für das IT1 Handbuch. Also nicht wundern, falls es dir bekannt vorkommt.* 

Vereinfacht gesagt ist Git die Cloud für Programmierer. Sobald mehrere Entwickler am gleichen Quellcode (der in einem sog. „Repository“ liegt) arbeiten, ist es nötig, die Beiträge („Commits“) der einzelnen Beteiligten zu verwalten und zusammenzuführen. Zusätzlich ermöglicht Git mehrere Entwicklungszweige, „Branches“ genannt. So können in einem Zweig experimentelle Funktionen getestet werden. Sobald sich diese Funktionen in einem stabilen Zustand befinden, können sie mit einem anderen Branch zusammengeführt werden („Merge“). Dabei werden die Unterschiede beider Quellcodes verglichen und die jeweils aktuellsten Teile übernommen. Sehr wichtig und praktisch ist auch, dass jeder Commit, also jede Änderung am Quellcode gespeichert wird. So bleibt immer und uneingeschränkt die Möglichkeit zu einer beliebigen vorherigen Version zurückzukehren.

Git bietet – auch dank der vielen Erweiterungen – noch zahlreiche weitere Funktionen. Für die tägliche Nutzung ist Git als Kommandozeilenprogramm aber eher umständlich, weshalb vielfältige auf Git aufbauende Plattformen frei verfügbar sind. In erster Hinsicht stellen diese Plattformen einen Server zur Verfügung mit dem Entwickler Quellcode synchronisieren können. Zudem wird  eine Benutzeroberfläche zur Verfügung gestellt. So ziemlich alle Plattformen bieten ein „Issue“-System (ein Issue wird auch „Ticket“ genannt). Dabei handelt es sich um eine Art Forum für das Repository mit dem auf Bugs und mögliche Verbesserungen hingewiesen und darüber diskutiert werden kann. Das Repository mit den Quelldateien, das Issue-System und eventuelle weitere Funktionen bilden zusammen ein Projekt. 

Den Kern, nämlich Git selbst, rühren all diese Plattformen nicht an. Das führt dazu, dass i.d.R. jeder Client (also das Programm auf Ihrem Rechner, mit dem Sie den Code hoch- und runterladen mit jeder Plattform kompatibel ist. Das am KIT verwendete kommerzielle GitLab richtet sich an die Softwareentwicklung innerhalb von Firmen. Es unterstützt gängige Benutzerkontenverwaltungen, weshalb Sie sich mit Ihrem KIT Account anmelden können. Im Rahmen dieses Praktikums brauchen Sie nur einen Bruchteil der verfügbaren Funktionen. Sie müssen auch nur das mindeste einrichten; soweit wie möglich ist alles vorkonfiguriert.

### Aber der AK-MIT programmiert doch gar nix?

GitLab kann man auch für normale Dateien nutzen; nicht nur für Quellcode. Der einzige und wesentliche Unterschied ist, dass viele Dateien keine reinen Textdateien sind und GitLab deswegen nicht anzeigen kann, was genau geändert wurde. Office Dateien sind ein prominentes Beispiel hierfür, ein weiteres wären PDF Dateien. Das ist i.d.R. kein Problem, da es nur eine Darstellungssache ist. Doch wenn Konflikte auftreten (zwei Personen haben versucht die gleiche Datei zu ändern), kann Git das anders als bei Text nicht selbst lösen; die Änderungen müssen manuell zusammengeführt werden. 

Daher versuchen wir so weit es geht unsere Dokumentation (wie diesen Leitfaden) in reiner Textform zu halten. 

## Vorgehen

### Übersicht

Jede Änderung durchläuft grundsätzlich die gleichen Schritte: 

1. Erstellen eines eigenen Branches für diese Änderung
2. Durchführen der Arbeit
3. Erstellen eines Merge Requests
4. Review und ggf. Korrekturen durch andere
5. Bei Zustimmung Durchführung des Merge Requests

Da das Erstellen von Branches i.d.R. keine großen Probleme bereitet, und an anderen Stellen bestens erklärt ist, fokussieren wir uns auf die Merge Requests. 

### Commits

Commits sollten soweit wie möglich/praktisch immer nur eine Änderung beinhalten. Oft, nicht immer, macht es Sinn für einzelne Dateien einzelne Commits zu erstellen, die dann bei Bedarf auch leicht wieder _unabhängig_ voneinander rückgängig gemacht werden können. 

Beispiel: eine AK-MITlerin ändert die Farbe eines Logos und aktualisiert die Dokumente in denen das Logo verwendet wurde. Zusätzlich lädt sie ein neues Sitzungsprotokoll hoch. Dann macht es am meisten Sinn, das geänderte Logo sowie alle Dateien, in denen das Logo geändert wurde, in einem Commit abzuschicken, da sie zusammenhängen, und wenn, dann nur als ganzes rückgängig gemacht werden. Das Protokoll hingegen ist unabhängig und sollte separat committed werden. 

### Merge Request erstellen

Merge Requests können nur auf GitLab selbst erstellt werden (z.B. nicht aus GitHub Desktop heraus). Beim Erstellen eines Merge Requests wird ausgewählt, welcher Branch wohin überführt werden soll. Weiterhin bedarf es noch eines Titels und einer Beschreibung. 

Sobald der Merge Request erstellt wird, werden andere Mitglieder benachrichtigt. Das ist also in etwa das Äquivalent dazu eine Mail zu tippen mit der Bitte den Anhang korrekturzulesen. 

### Review

Nachdem der Merge Request erstellt wurde, werden die Mitglieder weiter über jede Änderung informiert. Wenn also jemand (anderes) einen Commit im gleichen Branch erstellt, werden erneut alle benachrichtigt und können sich die Änderungen anschauen. Unter Umständen kann es auch sinnvoll sein, einen neuen Branch zu erstellen für die eigenen Review-Änderungen, um nicht in Konflikt mit den anderen Reviewern zu geraten. 

Weiterhin können auch einfach Kommentare unter den Merge Request geschrieben werden, über die andere ebenfalls informiert werden. 

### Merge Request durchführen

Sind die Änderungen abgesegnet, kann der Merge Request durchgeführt werden. Je nachdem wie ist die Durchführung auf wenige Mitglieder beschränkt und muss durch diese geschehen. 

## Beispiel Word Dokument

Wie oben erwähnt, lassen sich bei Word Dokumenten die Änderungen nicht direkt auf GitLab betrachen. Glücklicherweise bringt Word eine eigene Funktion mit um Änderungen sichtbar zu machen. 

Nachfolgend sind die einzelne Schritte aufgelistet, wobei nicht jedes mal explizit dabei steht, dass ein Commit erstellt werden muss. Bitte daran denken, den Commits aussagekräftige Beschreibungen zu geben. 

1. Erstelle einen eigenen Branch für deine Änderung. Nennen wir diesen Branch z.B. "sitzung-protokoll-2042-11-11". Wechsle für die nachfolgenden Schritte zu diesem Branch in GitHub Desktop. 
2. Stelle sicher, dass im Word Dokument die Funktion "Änderungen nachverfolgen" aktiviert ist. Sie findet sich in Word in der Leiste "Überprüfen". Es allerdings ist oft sinnvoll Formatierungsänderungen nicht nachzuverfolgen; dies kann deaktiviert werden. 
3. Führe alle gewünschten Änderungen durch. 
4. Falls ein Inhaltsverzeichnis vorhanden ist: Deaktiviere "Änderungen nachverfolgen" und aktualisiere das Inhaltsverzeichnis. Anschließend unbedingt "Änderungen nachverfolgen" wieder aktivieren. Analog für Tabellenverzeichnisse, Querverweise, etc. 
5. Erstelle auf GitLab einen Merge Request von "sitzung-protokoll-2042-11-11" in "main". Dabei auswählen, dass der Quellbranch gelöscht werden soll. Je nachdem was geändert wurde, kann die Beschreibung mehr oder weniger umfangreich ausfallen.
6. Mindestens eine weitere Person begutachtet die Änderungen. In Word kann derjenige direkt die Änderungen annehmen/ablehnen, und ggf. weitere Änderungen hinzufügen. In dem Fall sind obige Schritte zu wiederholen. 
7. Sind alle Änderungen angenommen (also reviewed) worden, steht dem Merge nichts mehr im Weg. Bei Bedarf kann jetzt noch ein PDF Dokument erstellt werden:
	* "Änderungen nachverfolgen" deaktivieren und PDF erstellen. Bei der PDF Erstellung muss in den Optionen "Dokument mit Markups" abgewählt werden, andernfalls werden auch die Kommentare ins PDF exportiert. Die Deaktivierung der Nachverfolgung ist nötig, da ansonsten alle Verweise im Dokument als geändert markiert werden. Nachdem das PDF erstellt wurde, unbedingt "Änderungen nachverfolgen" wieder aktivieren. 
