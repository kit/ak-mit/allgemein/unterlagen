# Unterlagen

Hier sind alle Dokumente des AK-MIT gesammelt. Genau wie unsere Sitzungen sind auch die Dokumente öffentlich soweit wie möglich. 

## Namensschema

Beim Anlegen neuer Dateien bitte auf nachfolgende Richtlinien achten, damit alle Dokumente einheitlich benannt sind. 

Diese Richtlinien gelten grundsätzlich auch für die Unterlagen der einzelnen Gremien.

### Versionen

Es müssen und sollen keine unterschiedliche Versionen der gleichen Datei abgelegt werden. Alle Dateien hier werden automatisch durch git versioniert. Tags, Branches und Commits im Allgemeinen bieten bessere Möglichkeiten zwischen Versionen zu wechseln. 

### Datum

Ähnlich wie die Dateiversionen wird auch das Änderungsdatum der Dateien automatisch erfasst und muss daher nicht im Dateiname stehen. 

Prominente Ausnahme sind die Unterlagen von Sitzungen. Da dort das Datum das einzige Merkmal ist, das es erlaubt die Sitzungen zu unterscheiden, steht es stets im Namen. 
